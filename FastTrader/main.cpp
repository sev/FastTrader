// TraderBase.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#ifdef PY_CTP_TRADER_EXPORTS
#include <boost/python.hpp>
#endif
#include "../ConfigLib/BrokerConfigReader.h"
#include "../ConfigLib/UserConfigReader.h"
#include "../ConfigLib/DataStoreConfigReader.h"

#include "../Log/logging.h"
#include "../ServiceLib/UserApiMgr.h"
#include "../SimpleStrategyLib/StrategyMgr.h"
#include "../SimpleStrategyLib/IStrategy.h"
#include "../SimpleStrategyLib/qt_post.h"
#include "../ServiceLib/Trader.h"
#ifdef HTTP_MONITOR
#include "HTTP/https_monitor.h"
#endif

#include "../DataStoreLib/DataStoreFace.h"
#include "../FacilityBaseLib/DateTimeHelper.h"

#if _MSC_VER > 1800
#pragma comment(lib,"legacy_stdio_definitions.lib")
#endif

#include <functional>
#include <fstream>
#include <signal.h>

#include <boost/lambda/lambda.hpp>

#include <QApplication>

void OnExit(int sig)
{
	StrategyMgr::GetInstance().Stop();
	//UserApiMgr::GetInstance().Release();
	//DataSaverMgr::GetInstance().Stop();
}
#ifdef _WIN32
#include <windows.h>


BOOL CtrlHandler( DWORD fdwCtrlType )
{
	//DEBUG_METHOD();
	switch( fdwCtrlType )
	{
		// Handle the CTRL-C signal.
	case CTRL_C_EVENT:
		//LOGDEBUG( "Ctrl-C event" );
		Beep( 750, 300 );
		OnExit(SIGINT);
		return( TRUE );

		// CTRL-CLOSE: confirm that the user wants to exit.
	case CTRL_CLOSE_EVENT:
		Beep( 600, 200 );
		//LOGDEBUG( "Ctrl-Close event" );
		OnExit(SIGABRT);
		return( TRUE );

		// Pass other signals to the next handler.
	case CTRL_BREAK_EVENT:
		Beep( 900, 200 );
		//LOGDEBUG( "Ctrl-Break event" );
		return FALSE;

	case CTRL_LOGOFF_EVENT:
		Beep( 1000, 200 );
		//LOGDEBUG( "Ctrl-Logoff event" );
		return FALSE;

	case CTRL_SHUTDOWN_EVENT:
		Beep( 750, 500 );
		//LOGDEBUG( "Ctrl-Shutdown event" );
		return FALSE;

	default:
		return FALSE;
	}
}
#endif


#ifdef PY_CTP_TRADER_EXPORTS
int py_main()
{
#else
int main(int argc, char* argv[])
{
#ifdef _WIN32
	SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler,TRUE);
#else
	signal(SIGINT, OnExit);
	signal(SIGTERM, OnExit);
#endif
#endif
	LOGINIT("TraderApp");
	LOGDEBUG("免责声明:-------------入市有风险,投资需谨慎!!!------------");
// 	try
// 	{
// 		REDIS_INIT("127.0.0.1", 6379);
// 	}
// 	catch (const std::exception& e)
// 	{
// 		LOGDEBUG("Redis初始化异常:",e.what());
// 	}
	std::string user_xml = "user.xml";
	std::string broker_xml = "broker.xml";
#ifndef PY_CTP_TRADER_EXPORTS
	if (argc >=2)
	{
		//有命令行参数;
		user_xml = argv[1];
		if (argc>=3)
		{
			broker_xml = argv[2];
		}
	}
	QApplication app(argc, argv);
	qt_post::the_post();
#endif
	
	if (!BrokerConfigReader::GetInstance().Load(broker_xml))
	{
		LOGDEBUG("服务器配置文件加载失败!!!");
		return EXIT_FAILURE;
	}
	std::unordered_map<std::string,ServerInfo>& brokers = BrokerConfigReader::GetInstance().GetBrokers();
	if (brokers.empty())
	{
		LOGDEBUG("没有要登录的服务器!!!");
		return EXIT_FAILURE;
	}
	if (!UserConfigReader::GetInstance().Load(user_xml))
	{
		LOGDEBUG("用户配置文件加载失败!!!");
		return EXIT_FAILURE;
	}

	std::map<std::string,UserLoginParam>& users = UserConfigReader::GetInstance().GetUsers();
	if (users.empty())
	{
		LOGDEBUG("没有要登录的用户!!!");
		return EXIT_FAILURE;
	} 
	//读取数据存储项;
	if (!DataStoreConfigReader::GetInstance().Load(user_xml))
	{
		LOGDEBUG("DataStoreConfig File Load Failed!!!");
	}
	//初始化回调;
	auto m_pDataStoreFace = boost::make_shared<DataStoreFace>();
	std::map<std::string, DataStoreItem>& dataItems = DataStoreConfigReader::GetInstance().GetDataStores();

	if (dataItems.empty())
	{
		LOGDEBUG("DataStore Item not config!");
		boost::shared_ptr<IDataStore> ptrDataStore
			= IDataStore::CreateStore("./data", IDataStore::dbtypeSqlite3);
		m_pDataStoreFace->SetDataStore(ptrDataStore);
	}
	else
	{
		boost::shared_ptr<IDataStore> ptrDataStore
			= IDataStore::CreateStore(dataItems.begin()->second.Path.c_str(), IDataStore::dbtypeSqlite3);
		m_pDataStoreFace->SetDataStore(ptrDataStore);
	}
	m_pDataStoreFace->SetStoreToDB(false);
	//启动服务;
	if (!m_pDataStoreFace->start())
	{
		LOGDEBUG("DataStore Start Failed!");
	}

	user_api_callback callback;
	callback.sltInstrumentInfo = boost::bind(&DataStoreFace::UpdateInstrument, m_pDataStoreFace, boost::lambda::_1);
	callback.sltExchangeInfo = boost::bind(&DataStoreFace::UpdateExchange, m_pDataStoreFace, boost::lambda::_1);
	callback.sltOnTick = boost::bind(&DataStoreFace::UpdateTick, m_pDataStoreFace , boost::lambda::_1);
	callback.sltInstrumentStatus = boost::bind(&DataStoreFace::UpdateStatus, m_pDataStoreFace, boost::lambda::_1);
	UserApiMgr::GetInstance().SetCallback(callback);
	//策略的配置参数;
	std::vector<StrategyParam>& strategy_params=UserConfigReader::GetInstance().GetStrtegyParams();
	for (std::size_t s=0;s<strategy_params.size();++s)
	{
		std::map<std::string,UserLoginParam>::iterator uiter=users.find(strategy_params[s].UserID);
		if (uiter==users.end())
		{
			//LOGDEBUG("策略指定的用户没有找到,忽略该策略{}",strategy_params[s].UserID.c_str());
			continue;
		}
		//添加到要登录的用户列表;
		auto siter = brokers.find(uiter->second.Server);
		if ( uiter->second.BrokerID.empty() || siter==brokers.end())
		{
			//LOGDEBUG("没有用户:{}要登录的前置服务器，忽略该策略的用户!!!",uiter->second.UserID.c_str());
			continue;
		}
		//创建策略对象;
		boost::shared_ptr<IStrategy> pStrategy =
			StrategyMgr::GetInstance().AddStrategyPtr(strategy_params[s].StrategyID,
			strategy_params[s].StrategyPath);
		if (!pStrategy)
		{
			LOGDEBUG("策略加载失败，忽略这个策略...");
			continue;
		}
		//创建交易接口;
		UserApiMgr& uam = UserApiMgr::GetInstance();
		boost::shared_ptr<Trader> trader_api = 
			uam.CreateTrader(uiter->second,siter->second);
		if (!trader_api)
		{
			LOGDEBUG("策略的主账户用户登录失败，忽略这个策略...");
			continue;
		}
		//创建行情接口;
		boost::shared_ptr<Market> market_api =
			uam.CreateMarket(uiter->second, siter->second);
		pStrategy->SetDataStore(m_pDataStoreFace);
		pStrategy->SetInstruments(strategy_params[s].Instruments);
		pStrategy->SetMainTrader(trader_api, strategy_params[s].Available);
		pStrategy->SetMainMarket(market_api);
		pStrategy->SetStartDay(strategy_params[s].TradingDay);
		//登录子账号;

		std::vector<boost::shared_ptr<Trader> > subTraderId;
		auto& subUsers = strategy_params[s].SubUsers;
		for (auto k = subUsers.begin(); k!=subUsers.end(); ++k)
		{
			std::map<std::string,UserLoginParam>::iterator suiter=users.find(k->first);
			if (suiter==users.end())
			{
				//LOGDEBUG("策略指定的子账户{}没有找到,忽略该账户",strategy_params[s].SubUserID[k].c_str());
				continue;
			}
			//添加到要登录的用户列表;
			auto ssiter = brokers.find(suiter->second.Server);
			if ( suiter->second.BrokerID.empty() || ssiter==brokers.end())
			{
				//LOGDEBUG("没有子账户:{}要登录的前置，忽略该策略的子账户!!!",suiter->second.UserID.c_str());
				continue;
			}
			//创建交易接口;
			boost::shared_ptr<Trader> strader_api=
				UserApiMgr::GetInstance().CreateTrader(suiter->second, ssiter->second);
			subTraderId.push_back(strader_api);
		}
		pStrategy->SetSubTraders(subTraderId);
		pStrategy->Start();
	}
#ifndef PY_CTP_TRADER_EXPORTS
	app.setQuitOnLastWindowClosed(true);
	app.exec();
	OnExit(SIGINT);
	m_pDataStoreFace->stop();
#endif
	return 0;
}

#ifdef PY_CTP_TRADER_EXPORTS

void py_exit()
{
	OnExit(SIGINT);
	StrategyMgr::GetInstance().Join();
}

BOOST_PYTHON_MODULE(PyCtpTrader)
{
	using namespace boost::python;
	def("py_main", py_main);
	def("py_exit", py_exit);
}



#endif