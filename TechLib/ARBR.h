#pragma once
#include "TechLib.h"
#include "Technique.h"
//	人气意愿指标ARBR
class TECH_API  CARBR : public TechnicalIndicator
{
public:
	// Constructors
	CARBR( );
	CARBR( KdataContainer * pKData );
	virtual ~CARBR();

public:
	virtual	void clear( );

	// Attributes
	uint32_t		m_nDays;
	int		m_itsGoldenFork;
	int		m_itsDeadFork;
	int		m_itsSold;
	int		m_itsBought;
	virtual	void	SetDefaultParameters( );
	void	attach( CARBR & src );
	virtual	bool	IsValidParameters( );

	// Operations
    virtual	int		signal( size_t nIndex, uint32_t * pnCode = NULL );
    virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax );
    virtual	bool	calc( double * pAR, double *pBR, size_t nIndex, bool bUseLast );
};
