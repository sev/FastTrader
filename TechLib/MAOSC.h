#pragma once
#include "Technique.h"


//	移动平均摆动量MAOSC
class  CMAOSC : public TechnicalIndicator
{
public:
	// Constructors
	CMAOSC( );
	CMAOSC( KdataContainer * pKData );
	virtual ~CMAOSC();

public:
	virtual	void Clear( );

	// Attributes
	int		m_nDays1;
	int		m_nDays2;
	int		m_nMADays;
	int		m_itsGoldenFork;
	int		m_itsDeadFork;
	virtual	void	SetDefaultParameters( );
	void	AttachParameters( CMAOSC & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal( int nIndex, UINT * pnCode = NULL );
	virtual	bool	min_max_info(int nStart, int nEnd, double *pdMin, double *pdMax );
	virtual	bool	calc( double * pValue, int nIndex, bool bUseLast );
	virtual	bool	calc( double * pValue, double * pMA, int nIndex, bool bUseLast );
};



