#pragma once
#include "Technique.h"


//	资金流动指数MFI
class  CMFI : public TechnicalIndicator
{
public:
	// Constructors
	CMFI( );
	CMFI( KdataContainer * pKData );
	virtual ~CMFI();

public:
	virtual	void clear( );

	// Attributes
	int		m_nDays;
	int		m_itsLong;
	int		m_itsShort;
	virtual	void	SetDefaultParameters( );
	void	AttachParameters( CMFI & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal( int nIndex, UINT * pnCode = NULL );
	virtual	bool	min_max_info(int nStart, int nEnd, double *pdMin, double *pdMax );
	virtual	bool	calc( double * pValue, int nIndex, bool bUseLast );
};

