#include "TechUserDrv.h"
#include "Tech.h"
#include "Technique.h"

custom_indicator_loader & GetTechUserDrv( )
{
	static	custom_indicator_loader	g_techuserdrv;
	g_techuserdrv.LoadDriver( );
	return g_techuserdrv;
}

custom_indicator_loader::custom_indicator_loader()
{
	m_pfnGetTechUserCount	=	NULL;
	m_pfnGetTechUserInfo	=	NULL;
	m_pfnCalculate			=	NULL;
	m_pfnGetSignal			=	NULL;
	m_hDrv					=	NULL;
}

custom_indicator_loader::~custom_indicator_loader()
{
#ifdef	SP_WINDOWS
	if( m_hDrv )
		FreeLibrary( m_hDrv );
#endif
	m_pfnGetTechUserCount	=	NULL;
	m_pfnGetTechUserInfo	=	NULL;
	m_pfnCalculate			=	NULL;
	m_pfnGetSignal			=	NULL;
	m_hDrv					=	NULL;
}

bool custom_indicator_loader::LoadDriver( )
{
#ifdef	SP_WINDOWS
	if( !m_hDrv )
	{
		m_hDrv = ::LoadLibrary( _T("StkTech.dll") );
		if( m_hDrv )
		{
			m_pfnGetTechUserCount	= (UINT (WINAPI *)())GetProcAddress(m_hDrv,"_GetTechUserCount@0");
			m_pfnGetTechUserInfo	= (bool (WINAPI *)(UINT,PTECHUSER_INFO))GetProcAddress(m_hDrv,"_GetTechUserInfo@8");
			m_pfnCalculate			= (bool (WINAPI *)(UINT,PCALCULATE_INFO))GetProcAddress(m_hDrv,"_Calculate@8");
			m_pfnGetSignal			= (int (WINAPI *)(UINT,PCALCULATE_INFO))GetProcAddress(m_hDrv,"_GetSignal@8");
			if( !m_pfnGetTechUserCount )
				m_pfnGetTechUserCount	= (UINT (WINAPI *)())GetProcAddress(m_hDrv,"GetTechUserCount");
			if( !m_pfnGetTechUserInfo )
				m_pfnGetTechUserInfo	= (bool (WINAPI *)(UINT,PTECHUSER_INFO))GetProcAddress(m_hDrv,"GetTechUserInfo");
			if( !m_pfnCalculate )
				m_pfnCalculate			= (bool (WINAPI *)(UINT,PCALCULATE_INFO))GetProcAddress(m_hDrv,"Calculate");
			if( !m_pfnGetSignal )
				m_pfnGetSignal			= (int (WINAPI *)(UINT,PCALCULATE_INFO))GetProcAddress(m_hDrv,"GetSignal");
		}
	}
#endif

	return NULL != m_hDrv;
}

UINT custom_indicator_loader::GetTechUserCount( )
{
	if( !m_pfnGetTechUserCount )
		return 0;
	return( (*m_pfnGetTechUserCount)());
}

bool custom_indicator_loader::GetTechUserInfo( UINT nID, PTECHUSER_INFO pInfo )
{
	if( !m_pfnGetTechUserInfo )
		return false;
	return( (*m_pfnGetTechUserInfo)(nID-STT_USER_MIN,pInfo));
}

bool custom_indicator_loader::Calculate( UINT nID, PCALCULATE_INFO pInfo )
{
	if( !m_pfnCalculate )
		return false;
	return( (*m_pfnCalculate)(nID-STT_USER_MIN,pInfo));
}

int custom_indicator_loader::GetSignal( UINT nID, PCALCULATE_INFO pInfo )
{
	if( !m_pfnCalculate )
		return ITS_NOTHING;
	return( (*m_pfnGetSignal)(nID-STT_USER_MIN,pInfo));
}
