#pragma once
#include "TechLib.h"
#include "Technique.h"
//	3��6�չ���3-6BIAS
class TECH_API C36BIAS : public TechnicalIndicator
{
public:
	// Constructors
	C36BIAS( );
	C36BIAS( KdataContainer * pKData );
	virtual ~C36BIAS();

public:
	virtual	void clear( );

	// Attributes
	int		m_itsSold;
	int		m_itsBought;
	virtual	void	SetDefaultParameters( );
	void	attach( C36BIAS & src );
	virtual	bool	IsValidParameters( );

	// Operations
    virtual	int		signal( size_t nIndex, uint32_t * pnCode = NULL );
    virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax );
    virtual	bool	calc( double * pValue, size_t nIndex, bool bUseLast );
};
