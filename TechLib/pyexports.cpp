#include <boost/python.hpp>

#include "MA.h"

BOOST_PYTHON_MODULE(TechLib)
{
	using namespace boost::python;

	class_<TechnicalIndicator >("technical_indicator")
		.def("clear",&TechnicalIndicator::clear)
		.def("signal", &TechnicalIndicator::signal)
		;

	class_<CMA, bases<TechnicalIndicator> >("CMA")
		.add_property("m_nType",&CMA::m_nType)
		.add_property("m_adwMADays", &CMA::m_adwMADays)
		.add_property("m_itsGoldenFork", &CMA::m_itsGoldenFork)
		.add_property("m_itsDeadFork", &CMA::m_itsDeadFork)
		.add_property("m_itsLong", &CMA::m_itsLong)
		.add_property("m_itsShort", &CMA::m_itsShort)
		;

}