#include "KLine.h"
#include <math.h>
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"

//////////////////////////////////////////////////////////////////////
//	CKLine
CKLine::CKLine( )
{
	SetDefaultParameters( );
}

CKLine::CKLine( KdataContainer * pKData )
	: TechnicalIndicator( pKData )
{
	SetDefaultParameters( );
}

CKLine::~CKLine()
{
	clear( );
}

void CKLine::SetDefaultParameters( )
{
}

void CKLine::attach( CKLine & src )
{
}

bool CKLine::IsValidParameters( )
{
	return true;
}

void CKLine::clear( )
{
	TechnicalIndicator::clear( );
}

/***
	得到K线价格的从nStart到nEnd的最小值和最大值
*/
bool CKLine::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax)
{
	STT_ASSERT_GETMINMAXINFO( m_pKData, nStart, nEnd );

	double	dMin	=	-1;
	double	dMax	=	1;
	for( size_t k=nStart; k <= nEnd; k++ )
	{
		KDATA	& kd	=	m_pKData->at(k);
		if( nStart == k || dMin > kd.LowestPrice )	dMin	=	kd.LowestPrice;
		if( nStart == k || dMax < kd.HighestPrice )	dMax	=	kd.HighestPrice;
	}
// 	dMin	-=	fabs(dMin) * 0.01;
// 	dMax	+=	fabs(dMax) * 0.01;
// 	if( dMin <= 0 )
// 		dMin	=	0;
// 	if( dMax - dMin < 0.03 )
// 		dMax	=	dMin + 0.03;

	if( pdMin )		*pdMin	=	dMin;
	if( pdMax )		*pdMax	=	dMax;
	return true;
}
