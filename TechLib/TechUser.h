#ifndef _TECH_USER_H_
#define _TECH_USER_H_
#include "TechLib.h"
#include "Technique.h"
#include <PlatformStruct.h>
// 用户外挂指标
class InstrumentData;

class TECH_API custom_indicator : public TechnicalIndicator
{
public:
	// Constructors
	custom_indicator( );
	custom_indicator( KdataContainer * pKData );
	virtual ~custom_indicator();

	// static functions
	static	UINT		GetTechUserCount();
	static	std::string	GetTechUserName(int nTech);
	static	std::string	GetTechUserShortName( int nTech );

	// techuser attributes
	bool		SetTechUserID( int nTechUserID );
	UINT		GetTechUserID( );
	void		SetInstrument( InstrumentData * pInstrument );
	UINT		GetLineCount( );
	char*	GetLineName( int nLine );

public:
	virtual	void	clear( );			//	清除

public:
	// Attributes
	virtual	void	SetDefaultParameters( );		// 设定缺省参数
	void	attach( custom_indicator & src );	// 指定参数与src相同
	virtual	bool	IsValidParameters( );			// 参数是否合法

	// Operations
    virtual	int		signal( size_t nIndex, uint32_t * pnCode = NULL );	// 得到买卖信号
    virtual	int		intensity( size_t nIndex, uint32_t * pnCode = NULL, bool bTraceBack = true );	// 得到买卖趋势信号
    virtual	int		intensity_traceback( size_t nIndex, uint32_t * pnCode = NULL );					// 得到买卖趋势信号
    virtual	bool	min_max_info( size_t nStart, size_t nEnd, double *pdMin, double *pdMax );		// 得到最大最小值
    virtual	bool	calc( double * pValue, size_t nIndex, bool bUseLast );					// 计算指标值
    virtual	bool	calc( double * pValue1, double * pValue2, size_t nIndex, bool bUseLast );		// 计算指标值
    virtual	bool	calc( double * pValue1, double * pValue2, double * pValue3, size_t nIndex, bool bUseLast );		// 计算指标值
    virtual	bool	calc( double * pValue1, double * pValue2, double * pValue3, double * pValue4, size_t nIndex, bool bUseLast );		// 计算指标值
    virtual	bool	calc( double * pValue1, double * pValue2, double * pValue3, double * pValue4, double * pValue5, size_t nIndex, bool bUseLast );		// 计算指标值

protected:
    size_t			m_nTechUserID;
	TECHUSER_INFO	m_techuserinfo;
	InstrumentData *		m_pInstrument;
};
#endif
