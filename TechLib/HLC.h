#pragma once
#include "Technique.h"

//	���ȷ���HLC
class TECH_API CHLC : public TechnicalIndicator
{
public:
	// Constructors
	CHLC( );
	CHLC( KdataContainer * pKData );
	virtual ~CHLC();

public:
	virtual	void clear( );

	// Attributes
	int		m_nDays;
	int		m_nMADays;
	int		m_itsGoldenFork;
	int		m_itsDeadFork;
	virtual	void	SetDefaultParameters( );
	void	AttachParameters( CHLC & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal( int nIndex, UINT * pnCode = NULL );
	virtual	bool	min_max_info(int nStart, int nEnd, double *pdMin, double *pdMax);
	virtual	bool	calc( double * pValue, int nIndex, bool bUseLast );
	virtual	bool	calc(double * pValue, double * pMA, int nIndex, bool bUseLast);
};

