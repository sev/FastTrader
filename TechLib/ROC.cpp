#include "ROC.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
//////////////////////////////////////////////////////////////////////
//	CROC
CROC::CROC( )
{
	SetDefaultParameters( );
}

CROC::CROC( KdataContainer * pKData )
	: TechnicalIndicator( pKData )
{
	SetDefaultParameters( );
}

CROC::~CROC()
{
	clear( );
}

void CROC::SetDefaultParameters( )
{
	m_nDays		=	10;
	m_nMADays	=	10;

	m_itsGoldenFork			=	ITS_BUY;
	m_itsDeadFork			=	ITS_SELL;
}

void CROC::attach( CROC & src )
{
	m_nDays		=	src.m_nDays;
	m_nMADays	=	src.m_nMADays;

	m_itsGoldenFork			=	src.m_itsGoldenFork;
	m_itsDeadFork			=	src.m_itsDeadFork;
}

bool CROC::IsValidParameters( )
{
	return ( VALID_DAYS(m_nDays) && VALID_DAYS(m_nMADays)
		&& VALID_ITS(m_itsGoldenFork) && VALID_ITS(m_itsDeadFork) );
}

void CROC::clear( )
{
	TechnicalIndicator::clear( );
}

int CROC::signal( size_t nIndex, uint32_t * pnCode )
{
	if( pnCode )	*pnCode	=	ITSC_NOTHING;

	int	nMaxDays	=	m_nDays + m_nMADays;
	double	dLiminalLow = 0, dLiminalHigh = 0;
	if( !intensity_prepare( nIndex, pnCode, nMaxDays, ITS_GETMINMAXDAYRANGE, &dLiminalLow, &dLiminalHigh, 0.309, 0.6 ) )
		return ITS_NOTHING;

	double	dROC, dMA;
	if( !calc( &dROC, &dMA, nIndex, false ) )
		return ITS_NOTHING;

	if( dROC < dLiminalLow && dMA < dLiminalLow && is_golden_fork( nIndex, m_pdCache1, m_pdCache2 ) )
	{	// 低位金叉
		if( pnCode )	*pnCode	=	ITSC_GOLDENFORK;
		return m_itsGoldenFork;
	}
	if( dROC > dLiminalHigh && dMA > dLiminalHigh && is_dead_fork( nIndex, m_pdCache1, m_pdCache2 ) )
	{	// 高位死叉
		if( pnCode )	*pnCode	=	ITSC_DEADFORK;
		return m_itsDeadFork;
	}

	return	ITS_NOTHING;
}

bool CROC::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax )
{
	return AfxGetMinMaxInfo2( nStart, nEnd, pdMin, pdMax, this );
}

/***
	ROC=（今收盘-前N日收盘）÷前N日的收盘×100
*/
bool CROC::calc( double * pValue, size_t nIndex, bool bUseLast )
{
	STT_ASSERT_CALCULATE1( m_pKData, nIndex );

	if( m_nDays > nIndex )
		return false;

	if( load_from_cache( nIndex, pValue ) )
		return true;

	if( m_pKData->MaindataAt(nIndex-m_nDays) <= 0
		|| m_pKData->MaindataAt(nIndex) <= 0 )
		return false;

	double	x	=	m_pKData->MaindataAt(nIndex);
	double	y	=	m_pKData->MaindataAt(nIndex-m_nDays);
	if( pValue )
		*pValue	=	(x - y) * 100 / y;
	store_to_cache( nIndex, pValue );
	return true;
}

bool CROC::calc(double * pValue, double * pMA, size_t nIndex, bool bUseLast )
{
	return calc_ma( pValue, pMA, nIndex, bUseLast, m_nMADays );
}
