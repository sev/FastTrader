#include "BBI.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
//////////////////////////////////////////////////////////////////////
//	CBBI
CBBI::CBBI( )
{
	SetDefaultParameters( );
}

CBBI::CBBI( KdataContainer * pKData )
	: TechnicalIndicator( pKData )
{
	SetDefaultParameters( );
}

CBBI::~CBBI()
{
	clear( );
}

void CBBI::SetDefaultParameters( )
{
	m_nMA1Days	=	3;
	m_nMA2Days	=	6;
	m_nMA3Days	=	12;
	m_nMA4Days	=	24;
// 	m_itsGoldenFork	=	ITS_BUY;
// 	m_itsDeadFork	=	ITS_SELL;
}

void CBBI::attach( CBBI & src )
{
	m_nMA1Days	=	src.m_nMA1Days;
	m_nMA2Days	=	src.m_nMA2Days;
	m_nMA3Days	=	src.m_nMA3Days;
	m_nMA4Days	=	src.m_nMA4Days;
	m_itsGoldenFork	=	src.m_itsGoldenFork;
	m_itsDeadFork	=	src.m_itsDeadFork;
}

bool CBBI::IsValidParameters( )
{
	//return ( VALID_DAYS( m_nMA1Days ) && VALID_DAYS( m_nMA2Days )
	//	&& VALID_DAYS( m_nMA3Days ) && VALID_DAYS( m_nMA4Days )
	//	&& VALID_ITS(m_itsGoldenFork) && VALID_ITS(m_itsDeadFork) );
	return true;
}

void CBBI::clear( )
{
	TechnicalIndicator::clear( );
}

int CBBI::signal( int nIndex, UINT * pnCode )
{
	return 0;
	//if( pnCode )	*pnCode	=	ITSC_NOTHING;
	//if( nIndex <= 0 )
	//	return ITS_NOTHING;

	//double	dLiminalLow = 0, dLiminalHigh = 0;
	//if( !IntensityPreparePrice( nIndex, pnCode, 0, ITS_GETMINMAXDAYRANGE, &dLiminalLow, &dLiminalHigh, 0.4, 0.6 ) )
	//	return ITS_NOTHING;

	//double	dBBINow = 0, dBBILast = 0;
	//if( !Calculate( &dBBILast, nIndex-1, false )
	//	|| !Calculate( &dBBINow, nIndex, false ) )
	//	return ITS_NOTHING;

	//double	dNowHigh	=	m_pKData->ElementAt(nIndex).m_fHigh;
	//double	dNowLow		=	m_pKData->ElementAt(nIndex).m_fLow;
	//double	dNowClose	=	m_pKData->ElementAt(nIndex).m_fClose;
	//double	dLastHigh	=	m_pKData->ElementAt(nIndex-1).m_fHigh;
	//double	dLastLow	=	m_pKData->ElementAt(nIndex-1).m_fLow;
	//double	dLastClose	=	m_pKData->ElementAt(nIndex-1).m_fClose;

	//if( dNowClose < dLiminalLow && dLastLow < dBBILast && dNowLow > dBBINow )
	//{	// 低位趋势向上
	//	if( pnCode )	*pnCode	=	ITSC_GOLDENFORK;
	//	return m_itsGoldenFork;
	//}
	//if( dNowClose > dLiminalHigh && dLastHigh > dBBILast && dNowHigh < dBBINow )
	//{	// 高位趋势向下
	//	if( pnCode )	*pnCode	=	ITSC_DEADFORK;
	//	return m_itsDeadFork;
	//}
	//return	ITS_NOTHING;
}

bool CBBI::min_max_info(int nStart, int nEnd,
				   double *pdMin, double *pdMax )
{
	return AfxGetMinMaxInfo1( nStart, nEnd, pdMin, pdMax, this );
}

/***
	BBI = 4 个 不同日期的MA 的平均值
*/
bool CBBI::calc( double * pValue, int nIndex, bool bUseLast )
{
	STT_ASSERT_CALCULATE1( m_pKData, nIndex );

	if( load_from_cache( nIndex, pValue ) )
		return true;
	
	double	dResult	=	0;
	double	dTemp	=	0;

	if( !m_pKData->GetMA( &dTemp, nIndex, m_nMA1Days ) )
		return false;
	dResult	+=	dTemp;

	if( !m_pKData->GetMA( &dTemp, nIndex, m_nMA2Days ) )
		return false;
	dResult	+=	dTemp;

	if( !m_pKData->GetMA( &dTemp, nIndex, m_nMA3Days ) )
		return false;
	dResult	+=	dTemp;

	if( !m_pKData->GetMA( &dTemp, nIndex, m_nMA4Days ) )
		return false;
	dResult	+=	dTemp;

	dResult	=	dResult / 4;
	if( pValue )
		*pValue	=	dResult;

	store_to_cache( nIndex, pValue );
	return true;
}
