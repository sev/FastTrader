#include "MTM.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
//////////////////////////////////////////////////////////////////////
//	CMTM
CMTM::CMTM( )
{
	SetDefaultParameters( );
}

CMTM::CMTM( KdataContainer * pKData )
	: TechnicalIndicator( pKData )
{
	SetDefaultParameters( );
}

CMTM::~CMTM()
{
	clear( );
}

void CMTM::SetDefaultParameters( )
{
	m_nDays		=	10;
	m_nMADays	=	10;

	m_itsGoldenFork			=	ITS_BUY;
	m_itsDeadFork			=	ITS_SELL;
}

void CMTM::attach( CMTM & src )
{
	m_nDays		=	m_nDays;
	m_nMADays	=	m_nMADays;

	m_itsGoldenFork			=	src.m_itsGoldenFork;
	m_itsDeadFork			=	src.m_itsDeadFork;
}

bool CMTM::IsValidParameters( )
{
	return ( VALID_DAYS( m_nDays ) && VALID_DAYS( m_nMADays )
		&& VALID_ITS(m_itsGoldenFork) && VALID_ITS(m_itsDeadFork) );
}

void CMTM::clear( )
{
	TechnicalIndicator::clear( );
}

int CMTM::signal( size_t nIndex, uint32_t * pnCode )
{
	if( pnCode )	*pnCode	=	ITSC_NOTHING;

	int	nMaxDays	=	m_nDays + m_nMADays;
	double	dLiminalLow = 0, dLiminalHigh = 0;
	if( !intensity_prepare( nIndex, pnCode, nMaxDays, ITS_GETMINMAXDAYRANGE, &dLiminalLow, &dLiminalHigh, 0.3, 0.6 ) )
		return ITS_NOTHING;

	double	dMTM, dMA;
	if( !calc( &dMTM, &dMA, nIndex, false ) )
		return ITS_NOTHING;

	if( dMTM < dLiminalLow && dMA < dLiminalLow && is_golden_fork( nIndex, m_pdCache1, m_pdCache2 ) )
	{	// 低位金叉
		if( pnCode )	*pnCode	=	ITSC_GOLDENFORK;
		return m_itsGoldenFork;
	}
	if( dMTM > dLiminalHigh && dMA > dLiminalHigh && is_dead_fork( nIndex, m_pdCache1, m_pdCache2 ) )
	{	// 高位死叉
		if( pnCode )	*pnCode	=	ITSC_DEADFORK;
		return m_itsDeadFork;
	}

	return	ITS_NOTHING;
}

bool CMTM::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax )
{
	return AfxGetMinMaxInfo2( nStart, nEnd, pdMin, pdMax, this );
}

/***
	MTM ＝ C/Cn
	其中：C为当日收市价，Cn为Ｎ日前收市价，
	N为设定参数，一般选设10日，亦可在6日至14日之间选择。
*/
bool CMTM::calc( double * pValue, size_t nIndex, bool bUseLast )
{
	STT_ASSERT_CALCULATE1( m_pKData, nIndex );
	
	if( m_nDays > nIndex )
		return false;

	if( load_from_cache( nIndex, pValue ) )
		return true;

	if( m_pKData->MaindataAt(nIndex-m_nDays) > 0 )
	{
		if( pValue )
			*pValue	=	100. * m_pKData->MaindataAt(nIndex) / m_pKData->MaindataAt(nIndex-m_nDays);
		store_to_cache( nIndex, pValue );
		return true;
	}
	return false;
}

bool CMTM::calc( double * pValue, double *pMA, size_t nIndex, bool bUseLast )
{
	return calc_ma( pValue, pMA, nIndex, bUseLast, m_nMADays );
}
