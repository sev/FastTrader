TARGET = TechLib
TEMPLATE = lib
CONFIG -= qt
INCLUDEPATH += $$PWD/../sdk/include
INCLUDEPATH += ../common
INCLUDEPATH += /home/rmb338/boost_1_64_0
INCLUDEPATH += ../spdlog/include
DEFINES += _USE_SPDLOG

DEFINES *= TECHLIB_EXPORTS
linux-g++|macx-g++{
QMAKE_LFLAGS= -m64 -Wall -DNDEBUG  -O2
QMAKE_CFLAGS = -arch x86_64 -lpthread
}
CONFIG += debug_and_release



CONFIG(debug, debug|release) {
        DESTDIR = ../build/debug
        LIBS  += -L$$PWD/../build/debug/ -lFacilityBaseLib
} else {
        DESTDIR = ../build/release
        LIBS = -L$$PWD/../build/release/ -lFacilityBaseLib
}

QMAKE_LFLAGS += /home/rmb338/boost_1_64_0/stage/lib/libboost_system.a
QMAKE_LFLAGS += /home/rmb338/boost_1_64_0/stage/lib/libboost_filesystem.a
QMAKE_LFLAGS += /home/rmb338/boost_1_64_0/stage/lib/libboost_thread.a
QMAKE_LFLAGS += /home/rmb338/boost_1_64_0/stage/lib/libboost_serialization.a
QMAKE_LFLAGS += /home/rmb338/boost_1_64_0/stage/lib/libboost_date_time.a

HEADERS += \
    WVAD.h \
    WR.h \
    VR.H \
    VOLUME.h \
    TechUserDrv.h \
    TechUser.h \
    TechParameter.h \
    Technique.h \
    TechLib.h \
    Tech.h \
    StringMgr.h \
    SAR.h \
    RSI.h \
    ROC.h \
    PV.h \
    PSY.h \
    OSC.h \
    OBV.h \
    MTM.h \
    MIKE.h \
    MACD.h \
    MA.h \
    KLine.h \
    KDJ.h \
    EMV.h \
    DRData.h \
    DMI.H \
    DJ.h \
    CW.h \
    CV.h \
    CR.h \
    CCI.h \
    BOLL.h \
    BIAS.h \
    BBI.h \
    ASI.H \
    ARBR.h

SOURCES += \
    WVAD.cpp \
    WR.cpp \
    VR.cpp \
    VOLUME.cpp \
    TechUserDrv.cpp \
    TechUser.cpp \
    TechParameter.cpp \
    Technique.cpp \
    Tech.cpp \
    SAR.cpp \
    RSI.cpp \
    ROC.cpp \
    PV.cpp \
    PSY.cpp \
    OSC.cpp \
    OBV.cpp \
    MTM.cpp \
    MIKE.cpp \
    MACD.cpp \
    MA.cpp \
    KLine.cpp \
    KDJ.cpp \
    EMV.cpp \
    DRData.cpp \
    DMI.cpp \
    DJ.cpp \
    CW.cpp \
    CV.cpp \
    CR.cpp \
    CCI.cpp \
    BOLL.cpp \
    BIAS.cpp \
    BBI.cpp \
    ASI.cpp \
    ARBR.cpp \
    36BIAS.cpp \
    VRSI.cpp \
    NVI.cpp \
    MFI.cpp \
    MOBV.cpp \
    MAOSC.cpp \
    KST.cpp \
    HSL.cpp \
    HLC.cpp \
    DPO.cpp \
    DPER.cpp \
    DMKI.cpp \
    DCYO.cpp \
    CYO.cpp \
    CI.cpp \
    CDP.cpp \
    ATR.cpp \
    AD.cpp \
    NVRSI.cpp \
    PCNT.cpp \
    PVI.cpp \
    REI.cpp \
    VMACD.cpp \
    UOS.cpp \
    VROC.cpp
