#ifndef _TECH_VOLUME_H_
#define _TECH_VOLUME_H_
#include "TechLib.h"
#include "Technique.h"

/////////////////////////////////////////////////////////////////////////
//	能量类
//	成交量
class TECH_API CVOLUME : public TechnicalIndicator
{
public:
	// Constructors
	CVOLUME( );
	CVOLUME( KdataContainer * pKData );
	virtual ~CVOLUME();

public:
	virtual	void clear( );

	// Attributes
	std::vector<uint32_t>	m_adwMADays;//时间
	int		m_itsDeviateOnBottom;
	int		m_itsDeviateOnTop;
	int		m_itsLong;
	int		m_itsShort;
	virtual	void	SetDefaultParameters( );
	void	attach( CVOLUME & src );
	virtual	bool	IsValidParameters( );

	// Operations
    virtual	int		signal( size_t nIndex, uint32_t * pnCode = NULL );
    virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax );
    virtual	bool	calc(double * pValue, size_t nIndex, size_t nDays, bool bUseLast );
};
#endif
