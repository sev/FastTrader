#include "RSI.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"

//////////////////////////////////////////////////////////////////////
//	CRSI
CRSI::CRSI( )
{
	SetDefaultParameters( );
}

CRSI::CRSI( KdataContainer * pKData )
: TechnicalIndicator( pKData )
{
	SetDefaultParameters( );
}

CRSI::~CRSI()
{
	clear( );
}

void CRSI::SetDefaultParameters( )
{
	m_adwDays.clear();
	m_adwDays.push_back( 10 );
	m_adwDays.push_back( 20 );
	m_itsSold		=	ITS_BUY;
	m_itsGoldenFork	=	ITS_BUYINTENSE;
	m_itsDeadFork	=	ITS_SELLINTENSE;
}

void CRSI::attach( CRSI & src )
{
	//m_adwDays.Copy( src.m_adwDays );
	//std::copy(m_adwDays.begin(),m_adwDays.end(),src.m_adwDays);
	m_adwDays=src.m_adwDays;
	m_itsSold		=	src.m_itsSold;
	m_itsGoldenFork	=	src.m_itsGoldenFork;
	m_itsDeadFork	=	src.m_itsDeadFork;
}

bool CRSI::IsValidParameters( )
{
	STT_VALID_DAYSARRAY( m_adwDays );
	return ( VALID_ITS(m_itsSold) && VALID_ITS(m_itsGoldenFork) && VALID_ITS(m_itsDeadFork) );
}

void CRSI::clear( )
{
	TechnicalIndicator::clear( );
}

int CRSI::signal( size_t nIndex, uint32_t * pnCode )
{
	if( pnCode )	*pnCode	=	ITSC_NOTHING;

	if( nIndex <= 0 )
		return ITS_NOTHING;

	int	nForkSignal	=	GetForkSignal(nIndex, m_adwDays, m_itsGoldenFork, m_itsDeadFork, pnCode );
	for( int k=0; k<m_adwDays.size(); k++ )
	{
		double	dRSI, dRSILast;
		if( !calc( &dRSILast, nIndex-1, m_adwDays[k], false )
			|| !calc( &dRSI, nIndex, m_adwDays[k], false ) )
			return ITS_NOTHING;
		if( dRSI < 20 )
		{	// 超卖
			if( pnCode )	*pnCode	=	ITSC_OVERSOLD;
			return m_itsSold;
		}
		if( dRSI < 40 && nForkSignal == m_itsGoldenFork )
		{	// 低位金叉
			if( pnCode )	*pnCode	=	ITSC_GOLDENFORK;
			return m_itsGoldenFork;
		}
		if( dRSI > 60 && nForkSignal == m_itsDeadFork )
		{	// 高位死叉
			if( pnCode )	*pnCode	=	ITSC_DEADFORK;
			return m_itsDeadFork;
		}
	}

	return	ITS_NOTHING;
}

bool CRSI::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax )
{
	if( pdMin )	*pdMin	=	0;
	if( pdMax )	*pdMax	=	100;
	return true;
}

/***
RSI =（N日内上涨幅度累计÷N日内上涨及下跌幅度累计）×100%
*/
bool CRSI::calc( double * pValue, size_t nIndex, size_t nDays, bool bUseLast )
{
	STT_ASSERT_CALCULATE( m_pKData, nIndex, nDays );

	if( nDays > nIndex )
		return false;

	double	dUC = 0, dDC = 0, dRSI = 0;
	int	nCount = 0;
	for( int k=nIndex; k>=1; k-- )
	{
		if( m_pKData->MaindataAt(k) > m_pKData->MaindataAt(k-1) )
			dUC	+=	( m_pKData->MaindataAt(k) - m_pKData->MaindataAt(k-1) );
		else
			dDC	+=	( m_pKData->MaindataAt(k-1) - m_pKData->MaindataAt(k) );

		nCount	++;
		if( nCount == nDays )
		{
			if( dUC+dDC < 1e-4 )
				dRSI	=	100;
			else
				dRSI	=	100*dUC/(dUC+dDC);
			if( pValue )	*pValue	=	dRSI;
			return true;
		}
	}
	return false;
}
