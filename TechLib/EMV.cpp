#include "EMV.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
//////////////////////////////////////////////////////////////////////
//	CEMV
CEMV::CEMV( )
{
	SetDefaultParameters( );
}

CEMV::CEMV( KdataContainer * pKData )
	: TechnicalIndicator( pKData )
{
	SetDefaultParameters( );
}

CEMV::~CEMV()
{
	clear( );
}

void CEMV::SetDefaultParameters( )
{
	m_nDays		=	14;
	m_nMADays	=	9;
	m_itsGoldenFork	=	ITS_BUY;
	m_itsGoldenFork	=	ITS_SELL;
}

void CEMV::attach( CEMV & src )
{
	m_nDays		=	src.m_nDays;
	m_nMADays	=	src.m_nMADays;
	m_itsGoldenFork	=	src.m_itsGoldenFork;
	m_itsDeadFork	=	src.m_itsDeadFork;
}

bool CEMV::IsValidParameters( )
{
	return ( VALID_DAYS(m_nDays) && VALID_DAYS(m_nMADays)
		&& VALID_ITS(m_itsGoldenFork) && VALID_ITS(m_itsDeadFork) );
}

void CEMV::clear( )
{
	TechnicalIndicator::clear( );
}

int CEMV::signal( size_t nIndex, uint32_t * pnCode )
{
	if( pnCode )	*pnCode	=	ITSC_NOTHING;

	int	nMaxDays	=	m_nDays+m_nMADays;
	double	dLiminalLow = 0, dLiminalHigh = 0;
	if( !intensity_prepare( nIndex, pnCode, nMaxDays, ITS_GETMINMAXDAYRANGE, &dLiminalLow, &dLiminalHigh, 0.5, 0.5 ) )
		return ITS_NOTHING;

	double	dEMV;
	if( !calc( &dEMV, nIndex, false ) )
		return ITS_NOTHING;

	int	nSignal	=	GetForkSignal( nIndex, m_itsGoldenFork, m_itsDeadFork, pnCode );
	if( dEMV < dLiminalLow && nSignal == m_itsGoldenFork )
	{	// 低位金叉
		if( pnCode )	*pnCode	=	ITSC_GOLDENFORK;
		return m_itsGoldenFork;
	}
	if( dEMV > dLiminalHigh && nSignal == m_itsDeadFork )
	{	// 高位死叉
		if( pnCode )	*pnCode	=	ITSC_DEADFORK;
		return m_itsDeadFork;
	}
	return	ITS_NOTHING;
/*  FROM BOOK */
/*	if( pnCode )	*pnCode	=	ITSC_NOTHING;

	double dValue;
	if( !Calculate( &dValue, nIndex, FALSE ) )
		return ITS_NOTHING;

	if( dValue > 0 )
	{
		if( pnCode )	*pnCode	=	ITSC_LONG;
		return m_itsLong;
	}
	if( dValue < 0 )
	{
		if( pnCode )	*pnCode	=	ITSC_SHORT;
		return m_itsShort;
	}
	
	return	ITS_NOTHING;
*/
}

bool CEMV::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax )
{
	return AfxGetMinMaxInfo2( nStart, nEnd, pdMin, pdMax, this );
}

/***
	A = （今天最高 + 今天最低）÷ 2
	B = （前一天最高 + 前一天最低）÷2
	C = 今天最高 - 今天最低
	EM = （A-B）×C÷今天成交额
	EMV = 累计n天的EM值
*/
bool CEMV::calc( double * pValue, size_t nIndex, bool bUseLast )
{
	STT_ASSERT_CALCULATE1( m_pKData, nIndex );

	if( m_nDays > nIndex )
		return false;

	if( load_from_cache( nIndex, pValue ) )
		return true;

	double	dEMV = 0;
	int	nCount	=	0;
	for( int k=nIndex; k>=1; k-- )
	{
		KDATA	kd		=	m_pKData->at(k);
		KDATA	kdLast	=	m_pKData->at(k-1);
		if( 0 == kd.Volume )
			return false;
		double	dDIF = 0;
		dDIF	=	(kd.HighestPrice+kd.LowestPrice)/2 - (((double)kdLast.HighestPrice)+kdLast.LowestPrice)/2;
		dEMV	+=	dDIF * (kd.HighestPrice-kd.LowestPrice) / kd.Volume;

		nCount	++;
		if( nCount == m_nDays )
		{
			if( pValue )
				*pValue	=	dEMV / m_nDays;
			store_to_cache( nIndex, pValue );
			return true;
		}
	}

	return false;
}

bool CEMV::calc( double * pValue, double * pMA, size_t nIndex, bool bUseLast )
{
	return calc_ma( pValue, pMA, nIndex, bUseLast, m_nMADays );
}
