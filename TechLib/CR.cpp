#include "CR.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"

//////////////////////////////////////////////////////////////////////
//	CCR
CCR::CCR( )
{
	SetDefaultParameters( );
}

CCR::CCR( KdataContainer * pKData )
: TechnicalIndicator( pKData )
{
	SetDefaultParameters( );
}

CCR::~CCR()
{
	clear( );
}

void CCR::SetDefaultParameters( )
{
	m_nDays		=	26;
	m_nMADaysA	=	10;
	m_nMADaysB	=	20;
	m_nMADaysC	=	40;
	m_nMADaysD	=	62;

	m_itsSold				=	ITS_BUY;
	m_itsBought				=	ITS_SELL;
}

void CCR::attach( CCR & src )
{
	m_nDays	=	src.m_nDays;
	m_nMADaysA	=	src.m_nMADaysA;
	m_nMADaysB	=	src.m_nMADaysB;
	m_nMADaysC	=	src.m_nMADaysC;
	m_nMADaysD	=	src.m_nMADaysD;

	m_itsSold				=	src.m_itsSold;
	m_itsBought				=	src.m_itsBought;
}

bool CCR::IsValidParameters( )
{
	return ( VALID_DAYS( m_nDays ) && VALID_DAYS( m_nMADaysA ) && VALID_DAYS( m_nMADaysB )
		&& VALID_DAYS( m_nMADaysC ) && VALID_DAYS( m_nMADaysD )
		&& VALID_ITS(m_itsSold) && VALID_ITS(m_itsBought) );
}

void CCR::clear( )
{
	TechnicalIndicator::clear( );
}

int CCR::signal( size_t nIndex, size_t * pnCode )
{
	if( pnCode )	*pnCode	=	ITSC_NOTHING;

	double	dCR;
	if( !calc( &dCR, nIndex, false ) )
		return ITS_NOTHING;

	if( dCR < 40 )
	{	// 超卖
		if( pnCode )	*pnCode	=	ITSC_OVERSOLD;
		return m_itsSold;
	}
	if( dCR > 300 )
	{	// 超买
		if( pnCode )	*pnCode	=	ITSC_OVERBOUGHT;
		return m_itsBought;
	}
	return	ITS_NOTHING;
}

bool CCR::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax )
{
	return AfxGetMinMaxInfo5( nStart, nEnd, pdMin, pdMax, this );
}

/***
MID = （开盘价 + 收盘价 + 最高价 + 最低价）÷ 2
上升值 = 今最高 - 昨日MID （负值记为0）
下跌值 = 昨MID - 今最低
n天上升值之和
CR = ———————— × 100
n天下跌值之和
*/
bool CCR::calc( double * pValue, size_t nIndex, bool bUseLast )
{
	STT_ASSERT_CALCULATE1( m_pKData, nIndex );

	if( m_nDays > nIndex )
		return false;

	if( load_from_cache( nIndex, pValue ) )
		return true;

	double	dBS = 0, dSS = 0, dCR = 0;
	int	nCount	=	0;
	for( int k=nIndex; k>=1; k-- )
	{
		KDATA	kd		=	m_pKData->at(k);
		KDATA	kdLast	=	m_pKData->at(k-1);
		double	dTP	=	(kdLast.HighestPrice+kdLast.LowestPrice+kdLast.OpenPrice+kdLast.ClosePrice)/4;
		dBS	+=	max( 0.0, kd.HighestPrice - dTP );
		dSS	+=	max( 0.0, dTP - kd.LowestPrice );

		nCount	++;
		if( nCount == m_nDays )
			break;
	}

	if( dSS < 1e-4 )
		return false;
	dCR	=	dBS * 100 / dSS;
	if( pValue )
		*pValue	=	dCR;
	store_to_cache( nIndex, pValue );
	return true;
}

/***
A, B, C, D 分别是CR的N日平均值
N分别为m_nMADaysA,m_nMADaysB,m_nMADaysC,m_nMADaysD
*/
bool CCR::calc( double * pValue, double * pA, double * pB, double * pC, double * pD, size_t nIndex, bool bUseLast )
{
	STT_ASSERT_CALCULATE1( m_pKData, nIndex );

	int	nMaxMADays	=	max( max(m_nMADaysA,m_nMADaysB), max(m_nMADaysC,m_nMADaysD) );
	if( m_nDays+nMaxMADays > nIndex+1 )
		return false;

	if( load_from_cache( nIndex, pValue, pA, pB, pC, pD ) )
		return true;

	double	dCR = 0, dA = 0, dB = 0, dC = 0, dD = 0;
	int	nCount	=	0;
	for( int k=nIndex; k>=m_nDays; k-- )
	{
		double	dTemp;
		if( calc( &dTemp, k, false ) )
		{
			if( nIndex == k )
				dCR	=	dTemp;
			nCount	++;
			if( nCount <= m_nMADaysA )
				dA	+=	dTemp;
			if( nCount <= m_nMADaysB )
				dB	+=	dTemp;
			if( nCount <= m_nMADaysC )
				dC	+=	dTemp;
			if( nCount <= m_nMADaysD )
				dD	+=	dTemp;

			if( nCount > nMaxMADays )
			{
				if( pValue )	*pValue	=	dCR;
				if( pA )	*pA	=	dA / m_nMADaysA;
				if( pB )	*pB	=	dB / m_nMADaysB;
				if( pC )	*pC	=	dC / m_nMADaysC;
				if( pD )	*pD	=	dD / m_nMADaysD;
				store_to_cache( nIndex, pValue, pA, pB, pC, pD );
				return true;
			}
		}
	}

	return false;
}
