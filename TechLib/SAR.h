#ifndef _TECH_SAR_H_
#define _TECH_SAR_H_
#include "TechLib.h"
#include "Technique.h"
//	停损点转向指标SAR
class TECH_API CSAR : public TechnicalIndicator
{
public:
	// Constructors
	CSAR( );
	CSAR( KdataContainer * pKData );
	virtual ~CSAR();

protected:
    bool	CalculateSAR(double * pValue, size_t nIndex, bool bUseLast);

public:
	virtual	void clear( );

	// Attributes
	uint32_t		m_nInitDays;
	uint32_t		m_bFirstUp;
	double	m_dAFStep;
	double	m_dAFMax;
	int		m_itsBuy;
	int		m_itsSell;
	virtual	void	SetDefaultParameters( );
	void	attach( CSAR & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal(size_t nIndex, uint32_t * pnCode = NULL);
	virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax);
	virtual	bool	calc( double * pValue, size_t nIndex, bool bUseLast );

protected:
	bool			m_bCurUp;
	bool			m_bTurn;
	double			m_dCurAF;
	double			m_dCurHigh;
	double			m_dCurLow;
};
#endif
