
#ifndef _TECH_VR_H_
#define _TECH_VR_H_
#include "TechLib.h"
#include "Technique.h"

class TECH_API  CVR : public TechnicalIndicator
{
public:
	// Constructors
	CVR( );
	CVR( KdataContainer * pKData );
	virtual ~CVR();

public:
	virtual	void clear( );

	// Attributes
	uint32_t		m_nDays;
	int		m_itsLong;
	int		m_itsShort;
	virtual	void	SetDefaultParameters( );
	void	attach( CVR & src );
	virtual	bool	IsValidParameters( );

	// Operations
    virtual	int		signal( size_t nIndex, uint32_t * pnCode = NULL );
    virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax );
    virtual	bool	calc( double * pValue, size_t nIndex, bool bUseLast );
};
#endif
