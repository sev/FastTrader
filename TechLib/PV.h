#ifndef _TECH_PV_H_
#define _TECH_PV_H_
#include "TechLib.h"
#include "Technique.h"
//	��֤������P/V
class TECH_API  CPV : public TechnicalIndicator
{
public:
	// Constructors
	CPV( );
	CPV( KdataContainer * pKData );
	virtual ~CPV();

public:
	virtual	void clear( );

	// Attributes
	virtual	void	SetDefaultParameters( );
	void	attach( CPV & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal( int nIndex, UINT * pnCode = NULL );
	virtual	bool	min_max_info( int nStart, int nEnd, double *pdMin, double *pdMax );
	virtual	bool	calc( double * pValue, int nIndex, bool bUseLast );
};
#endif