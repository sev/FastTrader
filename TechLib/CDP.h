#pragma once
#include "Technique.h"


//	���Ʋ���ָ��CDP
class TECH_API  CCDP : public TechnicalIndicator
{
public:
	// Constructors
	CCDP( );
	CCDP( KdataContainer * pKData );
	virtual ~CCDP();

public:
	virtual	void clear( );

	// Attributes
	virtual	void	SetDefaultParameters( );
	void	AttachParameters( CCDP & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal( size_t nIndex, uint32_t * pnCode = NULL );
	virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax);
	virtual	bool	calc(double * pAH, double * pNH, double * pAL, double * pNL, size_t nIndex, bool bUseLast);
};

