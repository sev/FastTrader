#ifndef _MACD_TECH_H_
#define _MACD_TECH_H_
#include "TechLib.h"
#include "Technique.h"
/////////////////////////////////////////////////////////////////////////
//	趋势类
//	指数平滑异同移动平均线MACD
class TECH_API  CMACD : public TechnicalIndicator
{
public:
	// Constructors
	CMACD( );
	CMACD( KdataContainer * pKData );
	virtual ~CMACD();

public:
	virtual	void clear( );

	// Attributes
	uint32_t		m_nEMA1Days;
	uint32_t		m_nEMA2Days;
	uint32_t		m_nDIFDays;
	int		m_itsDeviateOnBottom;
	int		m_itsDeviateOnTop;
	int		m_itsGoldenFork;
	int		m_itsDeadFork;
	virtual	void	SetDefaultParameters( );
	void	attach( CMACD & src );
	virtual	bool	IsValidParameters( );

	// Operations
    virtual	int		signal( size_t nIndex, uint32_t * pnCode = NULL );
    virtual	bool	min_max_info( size_t nStart, size_t nEnd, double *pdMin, double *pdMax );
	virtual	bool	calc( double *pdEMA1, double *pdEMA2, double *pdDIF, double *pdDEA,
		size_t nIndex, bool bUseLast );
};
#endif
