#ifndef _SIMULATION_H_
#define _SIMULATION_H_
#include "Strategy.h"


typedef	struct simulation_info_t	{
	HWND				hMainWnd;
	CStrategy	*		pStrategy;
} SIMULATION_INFO, * LPSIMULATION_INFO;


class TECH_API CSimulation
{
public:
	CSimulation(void);
	virtual ~CSimulation(void);

	static HANDLE m_hEventKillSimulationThread;
	static HANDLE m_hEventSimulationThreadKilled;

	void	SetStrategy( CStrategy * pStrategy, HWND hMainWnd );
	bool	DownloadDataIfNeed( );
	void	Restart( );
	void	Pause( );
	void	Continue( );
	void	Stop();
	void	OnEnd( bool bFinished );

protected:
	SIMULATION_INFO *	m_pSimulationInfo;
	CStrategy		*	m_pStrategy;
	HWND				m_hMainWnd;
	bool				m_bStopAndReset;
};
#endif
