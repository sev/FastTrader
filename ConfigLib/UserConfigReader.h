#ifndef  _USER_CONFIG_READER_H_
#define  _USER_CONFIG_READER_H_
#include "ConfigReader.h"
#include "../common/UserInfo.h"
#include "../common/DataTypes.h"

class CONFIGLIB_API UserConfigReader :
	public ConfigReader
{
public:
	UserConfigReader(void);
	virtual ~UserConfigReader(void);
	std::map<std::string,UserLoginParam>& GetUsers();
	std::vector<StrategyParam>& GetStrtegyParams();
protected:
	bool LoadUsers();
	bool LoadStrategyParams();
public:
	virtual bool Unload();
	virtual bool Load(const std::string& config_file="users.xml",
		bool bForceLoad=false);
protected:
	std::map<std::string,UserLoginParam> m_users;
	std::vector<StrategyParam> m_StrategyParams;
public:
	static UserConfigReader& GetInstance();
};

#endif
