﻿#ifndef _SQLITE3_OPERATOR_H_
#define _SQLITE3_OPERATOR_H_


//数据访问层使用OTL 实现;
#include "DataStoreLib.h"
#include "IDataStore.h"
//#include "../FacilityBaseLib/DateTime.h"
#include "sqlite3.h"
#include <stdexcept>

typedef std::logic_error sqlite_error ;
class DATASTORE_API Sqlite3Store:public IDataStore
{
protected:
	//Tick表;
	struct sqlite3 *mTickDB;
	std::string last_error_msg;
	//数据文件的名字;
	//合约表;
	struct sqlite3 *mInstDB;
	//K线数据表;
	struct sqlite3 *mKdataDB;
	//分时数据表;
	struct sqlite3 *mMinuteDB;
	//新闻数据表;
	struct sqlite3 *mNewsDB;

	//打开的数据库列表;
	std::map<std::string,struct sqlite3* > m_dbs;

	//配置参数;
	std::map<int, int> m_Options;
public:
	Sqlite3Store(void);
	virtual int Init(const std::string& initStr);
	virtual ~Sqlite3Store(void);

	//取得数据集的名称;
	virtual std::string GetRootDataSetName();
	//某个数据表是否已经存在;
	virtual int IsDataSetExists(std::string setName);
	//释放;
	virtual int Release();
	//设置数据库选项;
	virtual int SetOptions(int optionType,int optionValue);
	//字段列表;
    std::vector<std::string> GetDataSetFieldNames( std::string setName );

	// 得到临时目录;
	virtual	DWORD	GetSelfTempPath( char *szTempPath, int size );	
	// 得到最近错误;
	virtual	bool GetLastErrorMessage(char* lpszError, UINT nMaxError);	
	virtual	int	GetMaxNumber();		// 得到股票数量;
	
	//取函数;
	virtual	int	LoadCodetable( InstrumentContainer &container );	// 读取所有股票的信息;
	virtual	int	LoadKDataCache( InstrumentContainer &container, PROGRESS_CALLBACK fnCallback, void *cookie, int nProgStart, int nProgEnd );	// 读取所有股票的最近日线数据缓冲;
	virtual	int	LoadBasetable( InstrumentContainer & container );	// 读取某一股票的财务资料表，包括每股收益，每股净资产等，见CBaseData;
	virtual	int	LoadBaseText( InstrumentData *pstock );					// 读取某一股票的基本资料文本;
	virtual	int	LoadKData(InstrumentData *pstock, int nKType, int nLength = DEFAULT_LOAD_MIN1_COUNT);		// 读取某一股票的某个周期的K线数据;
	virtual int	LoadDRData( InstrumentData *pstock );					// 读取某一股票的除权除息资料;
	virtual int	LoadReport(InstrumentData *pstock, int nLength = DEFAULT_LOAD_TICK_COUNT);					// 读取某一股票的行情刷新数据;
	virtual int	LoadMinute(InstrumentData *pstock, int nLength = DEFAULT_LOAD_MIN1_COUNT);					// 读取某一股票的行情分时数据;
	virtual int	LoadOutline( InstrumentData *pstock );					// 读取某一股票的行情额外数据;
	virtual int LoadExchange(ExchangeContainer& exchanges);			// 读取交易所列表;
	//存函数;
	virtual int StoreExchange(ExchangeContainer& exchanges);
	virtual	int	StoreCodetable( InstrumentContainer & container );	// 保存代码表;
	virtual	int	StoreBasetable( InstrumentContainer & container );	//保存基本信息;
	virtual	int StoreDRData( InstrumentData *pstock );					// 保存某一股票的除权除息资料;
	virtual int	StoreReport( std::vector<boost::shared_ptr<Tick> > pReport, bool bBigTrade );	// 保存行情刷新数据;
	virtual int	StoreMinute(std::vector<boost::shared_ptr<MINUTE> > pMinute);	// 保存行情分时数据;
	virtual int	StoreOutline(std::vector<boost::shared_ptr<OUTLINE> > pOutline);	//

	//数据安装函数...;
	virtual	int	InstallCodetbl( const char * filename, const char *orgname );		// 安装下载的代码表;
	virtual	int	InstallCodetblBlock( const char * filename, const char *orgname );	// 安装下载的板块表;
	virtual	int	InstallKData( KdataContainer & kdata, bool bOverwrite = false );			// 安装K线数据;
	virtual	int	InstallBasetable( const char * filename, const char *orgname );		// 安装财务数据;
	virtual	int InstallBaseText( const char * filename, const char *orgname );		// 安装下载的基本资料数据，一只股票一个文件;
	virtual	int InstallBaseText( const char * buffer, int nLen, const char *orgname );		// 安装基本资料数据;

	virtual	int InstallNewsText( const char * filename, const char *orgname );		// 安装新闻数据文件;
	virtual	int InstallNewsText( const char * buffer, int nLen, const char *orgname );		// 安装新闻数据;

    virtual int GetDBTypeInfo(std::string& dbtypeName);
	virtual bool GetFileName( std::string &sFileName, int nDataType, InstrumentInfo * pInfo = NULL, int nKType = ktypeDay );
	virtual int StoreKData( KdataContainer & kdata, bool bOverwrite = false );
protected:
	//创建数据库;
	virtual bool CreateDB(const std::string& dbname,struct sqlite3*& db);
	//创建默认表;
	bool CreateTables();
	//数据库中是否有指定的表;
	virtual bool IsDataSetExists(const std::string& dbname,struct sqlite3* db);
	//K线数据表的名字;
	virtual std::string GetKDataTableName(const std::string& inst_id,int ktype);
	//错误信息;
	void SetLastError(const char* errmsg=nullptr,int errcode=0);
};

#endif
