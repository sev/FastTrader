#ifndef _DATA_FORMAT_H_
#define _DATA_FORMAT_H_

#include <complier.h>
#include <PlatformStruct.h>
#pragma	pack(1)

////////////////////////////////////////////////////////////////////
//	通用数据包的数据结构(*.STK)
typedef	struct tyday_fheader	{
	char	magic;
	char	code[6];
	char	magic2;
	char	name[8];
	DWORD	date;		//日期
	DWORD	datebegin;	//起始日期
	DWORD	dateend;	//终止日期
	DWORD	gendate;	//生成日期
	DWORD	gentime;	//生成时间
	DWORD	from;		//来源
	DWORD	sharecount;	//股票数
	DWORD	recordcount;//记录数
}TYDAY_FHEADER;

typedef	struct tyday_record	{
	char	magic;
	char	code[6];
	char	magic2;
	char	name[8];
	DWORD	date;		//日期
	DWORD	open;		//开盘(元/1000)
	DWORD	high;		//最高价(元/1000)
	DWORD	low;		//最低价(元/1000)
	DWORD	close;		//收盘(元/1000)
	DWORD	amount;		//成交额(千元)
	DWORD	volume;		//成交量(手)
	DWORD	serial;		//记录序号
}TYDAY_RECORD;

////////////////////////////////////////////////////////////////////
//	分析家数据包的数据结构(*.DAD)
#define	FXJDAY_FHEADER_MAGIC	0x8C19FC33
typedef	struct fxjday_fheader	{
	DWORD	m_dwMagic;			// 33 FC 19 8C，老版本的Magic
	DWORD	m_dwUnknown;		// 未知
	DWORD	m_dwStockCount;		// 为本文件的股票数
	DWORD	m_dwReserved;		// = 0
}FXJDAY_FHEADER;

typedef	union fxjday_record	{
	struct
	{
		DWORD	m_dwMagic;		// = -1
		WORD	m_wMarket;		// 市场 SH, SZ
		char	m_szCode[6];
		DWORD	m_dwReserved[5];
	};
	struct
	{
		time_t	m_time;		//日期
		float	open;		//开盘(元/1000)
		float	high;		//最高价(元/1000)
		float	low;		//最低价(元/1000)
		float	close;		//收盘(元/1000)
		float	volume;		//成交量(手)
		float	amount;		//成交额(元)
		float	reserved;	//
	};
}FXJDAY_RECORD;

#pragma	pack()
#ifdef _WIN32
#define CHAR_DIRSEP '\\'
#else
#define CHAR_DIRSEP '/'
#endif
#endif
