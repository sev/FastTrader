#if	!defined( __STKLIB_DATABASE_H__ )
#define	__STKLIB_DATABASE_H__
#include "DataStoreLib.h"
#include "../BasicLib/SpTime.h"
#include "../FacilityBaseLib/Instrument.h"
#include "../FacilityBaseLib/KData.h"
#include "DataFormat.h"
#include "IDataStore.h"
#include <vector>
#include <string>
using namespace std;

BOOL convert_TYDAY_RECORD_to_KDATA( TYDAY_RECORD * precord, KDATA * pkd );
BOOL convert_TYDAY_RECORD_MIN_to_KDATA( TYDAY_RECORD * precord, KDATA * pkd );
BOOL convert_KDATA_to_TYDAY_RECORD( DWORD dwSerial, const char * name, KDATA * pkd, TYDAY_RECORD * precord );
BOOL convert_KDATA_to_TYDAY_RECORD_MIN( DWORD dwSerial, const char * name, KDATA * pkd, TYDAY_RECORD * precord );

BOOL convert_FXJDAY_RECORD_to_KDATA( DWORD dwMarket, const char* lpszCode, int nKType, FXJDAY_RECORD *precord, KDATA *pkd );



#define	PROG_PROGRESS			1
#define	STKLIB_MAX_PROGRESS		10000
#define	STKLIB_MAXF_PROGRESS		10000.


#define	ERR_DB_MAX	1000
/***
	本地数据类，提供对磁盘数据的操作接口，包括股票信息，各种周期K线数据，基本
	资料，权息资料的读取和安装。
	
	使用方法如下：
	CStDatabase::CreateSelfDB( "./" );

	CStDatabase	db;
	db.SetRootPath( "./", IDataStore::dbtypeSelfDB );

	CStockContainer container;
	nLen	=	db.LoadCodetable( container );
	db.LoadBasetable( container );
	db.LoadKDataCache( container, NULL, NULL, 0, 0 );

	CInstrument	stock;
	stock.SetDatabase( &db );
	stock.PrepareData( CInstrument::dataK, kdata_container::ktypeDay );
	stock.PrepareData( CInstrument::dataDR );
	stock.PrepareData( CInstrument::dataK, m_nCurKType );
	CStockInfo	& info	=	stock.GetStockInfo();
	kdata_container	& kday	=	stock.GetKDataDay();
	kdata_container	& kdata	=	stock.GetKData(m_nCurKType);
	kday.SetDRData( stock.GetDRData() );
	kday.ChangeCurFormat( kdata_container::formatXDRdown, AfxGetProfile().GetAutoResumeDRBegin(), AfxGetProfile().GetAutoResumeDRLimit() );

	注意：使用CStDatabase的子类CNetDatabase也可以实现上述功能，并且提供的功能
	更多，使用方法相同。
*/
class DATASTORE_API CStDatabase /*: public Object*/
{
public:
	// Constructors
	CStDatabase();
	virtual ~CStDatabase();

	virtual int Init(string initStr){
		if (this->IsOK())
		{
			return m_pStore->Init(initStr);
		}
		return 0;
	}
	virtual int Release(){return 0;}
	// Operations
	static	bool CreateSelfDB( const char * rootpath );		// 创建所需目录
	static	bool IsValidDataType( int nType );				// 类型是否合法
	static	int	GetSupportedDataType ( CDBType * pdbtype, int maxsize );	// 得到支持的数据类型
	bool	SetRootPath( const char * rootpath, int nDBType = IDataStore::dbtypeUnknown );	// 设定数据目录

	bool	IsOK( );	// 数据目录是否符合
	const char * GetRootPath( );	// 得到数据目录
	int	GetDBType( );				// 得到数据类型
	const char * GetDBTypeName( );	// 得到数据类型名称

	virtual	DWORD	GetSelfTempPath( char *szTempPath, int size );	// 得到临时目录

	virtual	bool GetLastErrorMessage(char* lpszError, UINT nMaxError);	// 得到最近错误
	virtual	int	GetMaxStockNumber( );		// 得到股票数量
	virtual	int	LoadCodetable( CInstrumentContainer &container );	// 读取所有股票的信息
	virtual	int	StoreCodetable( CInstrumentContainer & container );	// 保存代码表
	virtual	int	LoadKDataCache( CInstrumentContainer &container, PROGRESS_CALLBACK fnCallback, void *cookie, int nProgStart, int nProgEnd );	// 读取所有股票的最近日线数据缓冲
	virtual	int	LoadBasetable( CInstrumentContainer & container );	// 读取某一股票的财务资料表，包括每股收益，每股净资产等，见CBaseData
	virtual	int	StoreBasetable( CInstrumentContainer & container );	// 保存某一股票的财务资料表
	virtual	int	LoadBaseText( CInstrument *pstock );					// 读取某一股票的基本资料文本
	virtual	int	LoadKData( CInstrument *pstock, int nKType );		// 读取某一股票的某个周期的K线数据
	virtual int	LoadDRData( CInstrument *pstock );					// 读取某一股票的除权除息资料
	virtual	int StoreDRData( CInstrument *pstock );					// 保存某一股票的除权除息资料
	virtual int	LoadReport( CInstrument *pstock );					// 读取某一股票的行情刷新数据
	virtual int	LoadMinute( CInstrument *pstock );					// 读取某一股票的行情分时数据
	virtual int	LoadOutline( CInstrument *pstock );					// 读取某一股票的行情额外数据
	virtual int	StoreReport( REPORT * pReport, int nCount, BOOL bBigTrade );	// 保存行情刷新数据
	virtual int	StoreMinute( MINUTE * pMinute, int nCount );	// 保存行情分时数据
	virtual int	StoreOutline( OUTLINE * pOutline, int nCount );	// 保存行情分时数据

	virtual	int	InstallCodetbl( const char * filename, const char *orgname );		// 安装下载的代码表
	virtual	int	InstallCodetblBlock( const char * filename, const char *orgname );	// 安装下载的板块表
	virtual	int	InstallCodetblFxjBlock( const char * filename, const char *orgname );	// 安装下载的分析家板块表
	virtual	int	InstallKData( kdata_container & kdata, BOOL bOverwrite = false );			// 安装K线数据
	virtual	int InstallKDataTy( const char * stkfile, int nKType, PROGRESS_CALLBACK fnCallback, void *cookie );	// 安装下载的K线通用格式数据
	virtual	int InstallKDataFxj( const char * dadfile, int nKType, PROGRESS_CALLBACK fnCallback, void *cookie );	// 安装下载的K线分析家格式数据
	//virtual	int InstallDRData( CDRData & drdata );									// 安装除权除息数据
	virtual	int	InstallDRDataClk( const char * filename, const char *orgname );		// 安装下载的除权除息数据，一只股票一个文件
	virtual	int	InstallDRDataFxj( const char * fxjfilename );						// 安装分析家除权除息数据
	virtual	int	InstallBasetable( const char * filename, const char *orgname );		// 安装财务数据
	virtual	int	InstallBasetableTdx( const char * filename );						// 安装通达信财务数据
	virtual	int	InstallBasetableFxj( const char * filename );						// 安装分析家财务数据
	virtual	int InstallBaseText( const char * filename, const char *orgname );		// 安装下载的基本资料数据，一只股票一个文件
	virtual	int InstallBaseText( const char * buffer, int nLen, const char *orgname );		// 安装基本资料数据
	virtual	int InstallNewsText( const char * filename, const char *orgname );		// 安装新闻数据文件
	virtual	int InstallNewsText( const char * buffer, int nLen, const char *orgname );		// 安装新闻数据


	// assistant operation
	bool	AddAssistantRootPath( const char * rootpath, int nDBType = IDataStore::dbtypeUnknown );	// 指定另外一个附加数据源的目录，自动读取
	void	RemoveAssistant( const char * rootpath );	// 移除另外一个附加数据源目录
	void	RemoveAllAssistant(  );						// 移除所有附加数据源目录

	static CSPTime	GetTimeInitial( );		// 得到初始数据日期
	bool	GetTimeLocalRange( CSPTime *ptmLatest, CSPTime * ptmPioneer, CSPTime * ptmInitial );	// 得到本地数据日期区间
	bool	GetNeedDownloadRange( CInstrumentInfo &info, CSPTime tmBegin, CSPTime tmEnd, CSPTime &tmDLBegin, CSPTime &tmDLEnd );	// 得到需要下载的数据日期区间

	// 将钱龙格式K线数据 加入 通用数据包
	static	int		AppendToTyData( const char *code, const char *name, int nKType, char* lpszKFile, char* lpszTyDataFile, time_t tmBegin, time_t tmEnd );
protected:
	IDataStore	* m_pStore;
	vector<void* >	m_aptrAssistant;
};

#endif	//__STKLIB_DATABASE_H__

