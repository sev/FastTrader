#ifndef OBSERVERWITHTHREAD_H
#define OBSERVERWITHTHREAD_H
#include "DataStoreLib.h"
#include <PlatformStruct.h>
#include <queue>
#include "../ServiceLib/Service/StkReceiver.h"
#include "../BasicLib/TinyThread/tinythread.h"


//在线程中运行的观察者;
class DATASTORE_API IObserverWithThread:public IObserver
{
public:
    IObserverWithThread();
    virtual ~IObserverWithThread();
protected:
    IObserverWithThread(const IObserverWithThread& other){}
    IObserverWithThread& operator=(const IObserverWithThread& other){return *this;}
public:
    virtual void start(bool bForceRestart=false);
    virtual void stop();
    virtual void join();
    virtual bool is_running();
    virtual int post_event(WPARAM wParam,LPARAM lParam)=0;
    virtual int handler(WPARAM wParam)=0;
	//标记每个线程的名字;
	virtual std::string name() const=0;
public:
    virtual int OnReceiverData(WPARAM wParam,LPARAM lParam);
    virtual bool isEnable();
protected:
    bool m_bRunning;
    std::queue<WPARAM> m_EventQueue;
    //线程相关函数;
    tthread::thread* m_thread;
    tthread::mutex m_mutex;
	tthread::mutex m_queue_mutex;
    tthread::condition_variable m_condvar;
    friend void thread_callback(void* thread_arg);
};

#endif // OBSERVERWITHTHREAD_H
