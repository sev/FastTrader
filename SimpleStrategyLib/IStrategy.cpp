#include "IStrategy.h"
#include "../../Log/logging.h"
#include "../ServiceLib/UserApiMgr.h"
#include "../ServiceLib/Market.h"
#include "../ServiceLib/Trader.h"
#include "../ServiceLib/InvestorAccount.h"
//#include "../ServiceLib/InvestorAccountMgr.h"
#include "StrategyImpl.h"
#include <typeinfo>



IStrategy::IStrategy(const std::string& id)
{
	m_pImpl=std::make_shared<StrategyImpl>(id);
	m_Version = "1.0";
	StrategyCallback scb;
	scb.OnTick=std::bind(&IStrategy::OnTick,this,std::placeholders::_1);
	scb.OnOrder=std::bind(&IStrategy::OnOrder,this,std::placeholders::_1);
	scb.OnTrade=std::bind(&IStrategy::OnTrade,this,std::placeholders::_1);
	scb.OnStatus=std::bind(&IStrategy::OnStatus,this,std::placeholders::_1);
	scb.OnOrderAction = std::bind(&IStrategy::OnOrderAction, this, std::placeholders::_1);
	scb.OnInitialze=std::bind(&IStrategy::Initialize,this);
	scb.OnFinalize=std::bind(&IStrategy::Finalize,this);
	m_pImpl->SetCallback(scb);
}


IStrategy::~IStrategy(void)
{
}

bool IStrategy::IsRun()
{
	return m_pImpl->IsRun();
}

void IStrategy::Start()
{
	m_pImpl->Start();
}


void IStrategy::Stop()
{
	m_pImpl->Stop();
}


const std::string& IStrategy::GetId() const
{
	return m_pImpl->GetId();
}


boost::shared_ptr<std::map<std::string, boost::shared_ptr<Order> > > IStrategy::GetOrder(boost::shared_ptr<Trader> traderID)
{
	return m_pImpl->GetOrder(traderID);
}

boost::shared_ptr<std::map<std::string, boost::shared_ptr<Trade> > > IStrategy::GetTrade(boost::shared_ptr<Trader> traderID)
{
	return m_pImpl->GetTrade(traderID);
}

boost::shared_ptr<std::map<std::string, boost::shared_ptr<Trade> > > IStrategy::GetTrades()
{
	return m_pImpl->GetTrades();
}

boost::shared_ptr<std::map<std::string, boost::shared_ptr<Order> > > IStrategy::GetOrders()
{
	//当前策略下的单;
	return m_pImpl->GetOrders();
}

boost::shared_ptr<InputOrder> IStrategy::OrderInsert( const std::string& instId,int Volume,double Price,
								  EnumOffsetFlag OpenOrClose,EnumDirection BuyOrSell,
	boost::shared_ptr<Trader> traderId)
{
	return m_pImpl->OrderInsert(instId,Volume,Price,OpenOrClose,BuyOrSell,traderId);
}

boost::shared_ptr<InputOrder> IStrategy::OrderInsert(boost::shared_ptr<InputOrder> order, boost::shared_ptr<Trader> traderId)
{
	return m_pImpl->OrderInsert(order, traderId);
}


boost::shared_ptr<InputOrderAction> IStrategy::OrderCancel( int frontId,int sessionId,const std::string& orderRef )
{
	return m_pImpl->OrderCancel(frontId,sessionId,orderRef);
}


boost::shared_ptr<InputOrderAction> IStrategy::OrderCancel(const std::string& ExchangeID, const std::string& OrderSysId, const std::string& instId)
{
	return m_pImpl->OrderCancel(ExchangeID,OrderSysId,instId);
}


boost::shared_ptr<InputOrderAction> IStrategy::OrderCancel(boost::shared_ptr<InputOrderAction> inputOrderAction, boost::shared_ptr<Trader> traderId)
{
	return m_pImpl->OrderCancel(inputOrderAction, traderId);
}



void IStrategy::SetMainTrader(boost::shared_ptr<Trader> uid, double Avaiable/* = 0*/)
{
	m_pImpl->SetMainTrader(uid, Avaiable);
}


boost::shared_ptr<Trader> IStrategy::GetMainTrader()
{
	return m_pImpl->GetMainTrader();
}


void IStrategy::PostEvent(const OnEventFun& evt)
{
	return m_pImpl->PostEvent(evt);
}


int IStrategy::StartTimer(OnEventFun& evt, int elapsed/*=1000*/)
{
	return m_pImpl->StartTimer(evt, elapsed);
}

bool IStrategy::StopTimer(int timerId)
{
	return m_pImpl->StopTimer(timerId);
}

void IStrategy::SetInstruments(const std::map<std::string, int>& instruments)
{
	m_pImpl->SetInstruments(instruments);
}

void IStrategy::OnTick(boost::shared_ptr<Tick> tick)
{

}

void IStrategy::Finalize()
{
}

void IStrategy::Initialize()
{
}

void IStrategy::OnOrder(boost::shared_ptr<Order> order)
{

}

void IStrategy::OnTrade(boost::shared_ptr<Trade> trade)
{

}


void IStrategy::OnOrderAction(boost::shared_ptr<InputOrderAction> orderAction)
{

}


void IStrategy::Restore()
{
	m_pImpl->Restore();
}

void IStrategy::OnStatus(boost::shared_ptr<InstStatus> status)
{

}

bool IStrategy::IsMyOrder(boost::shared_ptr<Order> order )
{
	return m_pImpl->IsMyOrder(order);
}

bool IStrategy::Join()
{
	return m_pImpl->Join();
}

void IStrategy::SetMainMarket(boost::shared_ptr<Market> ptrMarket)
{
	m_pImpl->SetMainMarket(ptrMarket);
}

void IStrategy::SetSubMarkets(const std::vector<boost::shared_ptr<Market> >& subId)
{
	m_pImpl->SetSubMarkets(subId);
}

void IStrategy::SetSubTraders( const std::vector<boost::shared_ptr<Trader> >& subId )
{
	m_pImpl->SetSubTraders(subId);
}

std::string IStrategy::GetInvestorIDByTraderID( const std::string& traderId )
{
	boost::shared_ptr<Trader> trader = UserApiMgr::GetInstance().GetTrader(traderId);
	if (trader)
	{
		return trader->GetUser().InvestorID;
	}
	return "";
}


boost::shared_ptr<Trader> IStrategy::GetTraderIDByFronIDSessionID( int frontId,int SessionId )
{
	return StrategyImpl::GetTraderIDByFronIDSessionID(frontId, SessionId);
}


bool IStrategy::IsMyTrade( boost::shared_ptr<Trade> trade )
{
	return m_pImpl->IsMyTrade(trade);
}

std::map<std::string,int>& IStrategy::GetInstruments() 
{
	return m_pImpl->GetInstruments();
}

void IStrategy::SubInstruments( const std::vector<std::string>& instruments, const std::string& exchange)
{
	return m_pImpl->SubInstruments(instruments,exchange);
}

void IStrategy::SubInstrument(const std::string & instrument, const std::string & exchange)
{
	std::vector<std::string> insts;
	insts.push_back(instrument);
	SubInstruments(insts,exchange);
}

void IStrategy::UnSubInstruments( const std::vector<std::string>& instruments, const std::string& exchange)
{
	return m_pImpl->UnSubInstruments(instruments,exchange);
}

void IStrategy::UnSubInstrument(const std::string & instrument, const std::string & exchange)
{
	std::vector<std::string> insts;
	insts.push_back(instrument);
	UnSubInstruments(insts, exchange);
}

boost::shared_ptr<DataStoreFace> IStrategy::GetDataStoreFace()
{
	return m_pImpl->GetDataStoreFace();
}


void IStrategy::SetDataStore(boost::shared_ptr<DataStoreFace> pDataStoreFace)
{
	return m_pImpl->SetDataStoreFace(pDataStoreFace);
}

bool IStrategy::CloseAllPositions(const std::string& InstrumentID)
{
	return m_pImpl->CloseAllPositions(InstrumentID);
}

bool IStrategy::SetEnableTrading(boost::shared_ptr<Trader> traderId, bool bEnableTrading /*= true*/)
{
	return m_pImpl->SetEnableTrading(traderId, bEnableTrading);
}

bool IStrategy::IsEnableTrading(boost::shared_ptr<Trader> traderId)
{
	return m_pImpl->IsEnableTrading(traderId);
}

boost::shared_ptr<InvestorPosition> IStrategy::GetPosition(const std::string& InstrumentID)
{
	return m_pImpl->GetPosition(InstrumentID);
}

int IStrategy::GetSignedPosition(const std::string& InstrumentID)
{
	return m_pImpl->GetSignedPosition(InstrumentID);
}

bool IStrategy::GetPositionCost(const std::string& InstrumentID, double& PositionAvgPrice)
{
	return m_pImpl->GetPositionCost(InstrumentID, PositionAvgPrice);
}

boost::shared_ptr<InputOrderAction> IStrategy::SafeCancelOrder(boost::shared_ptr<Order> order, boost::shared_ptr<InputOrder> inputOrder /*= nullptr*/)
{
	return m_pImpl->SafeCancelOrder(order, inputOrder);
}

boost::shared_ptr<InputOrderAction> IStrategy::SafeCancelOrder(boost::shared_ptr<InputOrder> order)
{
	return m_pImpl->SafeCancelOrder(order);
}

boost::shared_ptr<InputOrder> IStrategy::ClosePosition(const InvestorPosition& ip)
{
	return m_pImpl->ClosePosition(ip);
}

boost::shared_ptr<std::map<std::string, boost::shared_ptr<InputOrder> > > IStrategy::GetInputOrders()
{
	return m_pImpl->GetInputOrders();
}

boost::shared_ptr<std::map<std::string, boost::shared_ptr<InputOrder> > > IStrategy::GetInputOrder(boost::shared_ptr<Trader> traderID)
{
	return m_pImpl->GetInputOrder(traderID);
}

boost::shared_ptr<std::map<std::string, boost::shared_ptr<InputOrderAction> > > IStrategy::GetInputOrderActions()
{
	return m_pImpl->GetInputOrderActions();
}

boost::shared_ptr<std::map<std::string, boost::shared_ptr<InputOrderAction> > > IStrategy::GetInputOrderAction(boost::shared_ptr<Trader> traderID)
{
	return m_pImpl->GetInputOrderAction(traderID);
}

const std::string& IStrategy::GetFilePath() 
{
	return m_FilePath;
}

const std::string& IStrategy::GetName() 
{
	return m_Name;
}

void IStrategy::SetFilePath(const std::string& filePath)
{
	m_FilePath = filePath;
}

void IStrategy::SetName(const std::string& name)
{
	m_Name = name;
}

const std::string& IStrategy::GetVersion()
{
	return m_Version;
}

void IStrategy::SetVersion(const std::string& version)
{
	m_Version = version;
}

void IStrategy::SetStartDay(const std::string& startDay)
{
	m_pImpl->SetStartDay(startDay);
}

std::string IStrategy::GetStartDay()
{
	return m_pImpl->GetStartDay();
}

boost::shared_ptr < std::vector<boost::shared_ptr<InvestorPosition> > > IStrategy::GetPositions()
{
	return m_pImpl->GetPositions();
}

boost::shared_ptr < std::vector<boost::shared_ptr<TradingAccount> > > IStrategy::GetTradingAccounts()
{
	return m_pImpl->GetTradingAccounts();
}

boost::shared_ptr < std::vector<boost::shared_ptr<InvestorPosition> > > IStrategy::GetClosedPositions()
{
	return m_pImpl->GetClosedPositions();
}

boost::shared_ptr < std::vector<boost::shared_ptr<TradingAccount> > > IStrategy::GetTradingAccountsToday()
{
	return m_pImpl->GetTradingAccountsToday();
}

boost::shared_ptr<InputOrder> IStrategy::OpenPosition(const std::string& InstrumentID, EnumDirection BuyOrSell, int volume)
{
	return m_pImpl->OpenPosition(InstrumentID, BuyOrSell, volume, m_pImpl->GetMainTrader());
}

int IStrategy::GetCancelCount(const std::string& InstrumentID)
{
	return m_pImpl->GetCancelCount(InstrumentID);
}











