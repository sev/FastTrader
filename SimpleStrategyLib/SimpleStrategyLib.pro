TARGET = SimpleStrategyLib
TEMPLATE = lib
CONFIG -= qt

DEFINES *= SERVICELIB_EXPORTS
INCLUDEPATH += $$PWD/../sdk/include
INCLUDEPATH += ../common
INCLUDEPATH += /home/rmb338/boost_1_64_0
INCLUDEPATH += ../spdlog/include
INCLUDEPATH += /usr/include/python2.7
DEFINES += _USE_SPDLOG
CONFIG += debug_and_release c++11
linux-g++|macx-g++{
QMAKE_LFLAGS= -m64 -Wall -DNDEBUG  -O2
QMAKE_CFLAGS = -arch x86_64 -lpthread ../sdk/lib/thosttraderapi.so ../sdk/lib/thostmduserapi.so
}
CONFIG(debug, debug|release) {
        DESTDIR = ../build/debug
        LIBS  += -L$$PWD/../build/debug/ -lFacilityBaseLib -lTechLib
} else {
        DESTDIR = ../build/release
        LIBS = -L$$PWD/../build/release/ -lFacilityBaseLib -lTechLib
}

#$$PWD/../sdk/lib/thosttraderapi.so $$PWD/../sdk/lib/thostmduserapi.so

QMAKE_LFLAGS += /home/rmb338/boost_1_64_0/stage/lib/libboost_system.a
QMAKE_LFLAGS += /home/rmb338/boost_1_64_0/stage/lib/libboost_filesystem.a
QMAKE_LFLAGS += /home/rmb338/boost_1_64_0/stage/lib/libboost_thread.a
QMAKE_LFLAGS += /home/rmb338/boost_1_64_0/stage/lib/libboost_serialization.a
QMAKE_LFLAGS += /home/rmb338/boost_1_64_0/stage/lib/libboost_date_time.a

#unix|win32: LIBS += -l./thostmduserapi.so

#unix|win32: LIBS += -l./thostmduserapi.so

HEADERS += \
    csv_output.h \
    IStrategy.h \
    minicsv.h \
    StrategyImpl.h \
    StrategyMgr.h

SOURCES += \
    IStrategy.cpp \
    StrategyImpl.cpp \
    StrategyMgr.cpp




