#include "SimpleStrategy.h"
#include "../ServiceLib/OperatorManager.h"
#include "../ctp_platform/ctp_platform_server.h"
//#include "../FacilityBaseLib/Instrument.h"
#include <iostream>
#include <iosfwd>
using namespace std;


simple_strategy::simple_strategy(void)
	:m_monitor(NULL)
{
}


simple_strategy::~simple_strategy(void)
{
}

int simple_strategy::post_event( WPARAM wParam,LPARAM lParam )
{
	//DEBUG_METHOD();
	//DEBUG_PRINTF("simple_strategy::post_event:%d,%08x",wParam,lParam);
	switch(wParam)
	{
	case LoginFinished:
		{
			user_info_ptr pUserInfo=reinterpret_cast<user_info_ptr>(lParam);
			if (pUserInfo!=	NULL & m_current_user.user_id==pUserInfo->user_id)
			{
				tthread::lock_guard<tthread::mutex> slock(m_dataQueueMutex);
				m_qUsers.push(*pUserInfo);
				return NONE;
			}
		}
		break;
	case RtnDepthMarketData:
		{
			COMMPACKET* pReport=reinterpret_cast<COMMPACKET*>(lParam);
			if (pReport!=NULL && pReport->m_dwDataType==instrument::dataReport)
			{
				std::vector<std::string>::iterator  piter= 
					std::find(m_current_user.instruments.begin(),m_current_user.instruments.end(),
					pReport->m_pReport->szCode);
				if (piter!=m_current_user.instruments.end())
				{
					tthread::lock_guard<tthread::mutex> slock(m_dataQueueMutex);
					m_qRealtime.push(*(pReport->m_pReport));
					return NONE;
				}
			}
		}
		break;
	case LogoutFinished:
		{
			user_info_ptr pUserInfo=reinterpret_cast<user_info_ptr>(lParam);
			if (NULL!=pUserInfo)
			{
				tthread::lock_guard<tthread::mutex> slock(m_dataQueueMutex);
				m_qUsers.push(*pUserInfo);
				return NONE;
			}
		}
		break;
	case RtnOrder:
		{
			OrderField* rsp=reinterpret_cast<OrderField*>(lParam);
			if (NULL!=rsp)
			{
				tthread::lock_guard<tthread::mutex> slock(m_dataQueueMutex);
				m_qOrders.push(*rsp);
				return NONE;
			}
		}
		break;
	case RtnTrade:
		{
			Trade* rsp=reinterpret_cast<Trade*>(lParam);
			if (NULL!=rsp)
			{
				tthread::lock_guard<tthread::mutex> slock(m_dataQueueMutex);
				m_qTrades.push(*rsp);
				return NONE;
			}
			
		}
		break;
	case RtnInstrumentStatus:
		{
			InstrumentStatus* rsp=reinterpret_cast<InstrumentStatus*>(lParam);
			if (rsp)
			{
				tthread::lock_guard<tthread::mutex> slock(m_dataQueueMutex);
				m_qStatus.push(*rsp);
				return NONE;
			}
		}
		break;
	case instrument::dataBasetable:
		{
			if (m_current_user.instruments.empty())
			{
				m_InstrumentContainer.CopyData(instrument_container::get_instance());
				m_EventQueue.push(QryInstruments);
				return NONE;
			}
		}
	case QryInstruments:
		{
			if (m_current_user.instruments.empty())
			{
				return 1;
			}
			BASEDATA* pBaseData=reinterpret_cast<BASEDATA*>(lParam);
			if (pBaseData)
			{
				tthread::lock_guard<mutex> slock(m_dataQueueMutex);
				m_qBaseData.push(*pBaseData);
				return NONE;
			}
			return 1;
		}
	default:
		{
			return 1;
		}
		break;
	}
	return NONE;
}

bool simple_strategy::add_user(user_info& user,server_info& server)
{
	tthread::lock_guard<tthread::mutex> slock(m_mutex);
	//注册该用户的合约;
//	std::vector<user_info>::iterator usr_it=std::find(m_users.begin(),m_users.end(),user.user_id);
// 	if (usr_it==m_users.end())
// 	{
// 		//该用户还不存在;
// 		m_users.push_back(user);
// 		on_login(user);
// 	}
	m_current_user=user;
	m_current_user.auto_confirm=true;
	m_current_user.login_market=true;
	m_current_user.login_trader=true;
	m_server=server;
	get_service_dispatcher().AddRcvDataWnd(this);

	return true;
}

void simple_strategy::on_login(user_info& user)
{
	//查询合约;
	std::vector<std::string> instruments=m_current_user.instruments;
	m_current_user=user;
	m_current_user.instruments=instruments;
	user.instruments=instruments;
	platform_server* op=platform_manager::get_instance().Find(m_current_user.broker_id);
	if (NULL==op)
	{
		return ;
	}
	//1.如果持仓为空，则查自己要查的合约;
	if (!m_current_user.instruments.empty())
	{
		op->request(IObserver::QryInstruments,&m_current_user.instruments);
	}
	else
	{
		std::map<std::string,InvestorPositionDetail>::iterator positer=
			m_current_user.detail_positions.begin();
		while(positer!=m_current_user.detail_positions.end())
		{
			std::vector<std::string>::iterator iiter=std::find(m_current_user.instruments.begin(),
				m_current_user.instruments.end(),positer->second.InstrumentID);
			if (iiter==m_current_user.instruments.end())
			{
				m_current_user.instruments.push_back(positer->second.InstrumentID);
			}
			++positer;
		}
	}
	
	//计算资产序列;
	ASSETSERIAL asset={0};
	asset.dAsset+=m_current_user.account.DynamicBalance;
	asset.dCash +=m_current_user.account.StaticBalance;
	asset.time = time(NULL);
	m_AssetSerial.push_back(asset);

}

bool simple_strategy::del_user(const string& investor_id)
{
// 	std::vector<user_info>::iterator usr_it=std::find(m_users.begin(),m_users.end(),investor_id);
// 	if (usr_it!=m_users.end())
// 	{
// 		//该用户还不存在;
// 		m_users.erase(usr_it);
// 	}
	//发送登出请求;
	return true;
}

void simple_strategy::on_logout(user_info user)
{
	DEBUG_METHOD();

	//取消该用户的注册合约;
// 	std::vector<std::string> need_del_instruments;
// 	for (int j=0;j<user.instruments.size();++j)
// 	{
// 		bool bInstHasReg=false;
// 		for (int i=0;i<m_users.size();++i)
// 		{
// 			vector<string>::iterator inst_iter=std::find(m_users[i].instruments.begin(),
// 				m_users[i].instruments.end(),user.instruments[j]);
// 			if (inst_iter!=m_users[i].instruments.end())
// 			{
// 				//该合约已经注册;
// 				bInstHasReg=true;
// 				break;
// 			}
// 		}
// 		if (bInstHasReg==false)
// 		{
// 			need_del_instruments.push_back(user.instruments[j]);
// 		}
// 	}
// 	//取消订阅合约;
// 
// 	platform_server* op=(platform_server*)platform_manager::get_instance().Find(user.broker_id);
// 	if (NULL==op)
// 	{
// 		return ;
// 	}
// 	op->request(IObserver::RspUnSubcribeMarketData,&need_del_instruments);
}

void simple_strategy::on_broken()
{
	DEBUG_METHOD();
}


user_info simple_strategy::get_user()
{
	user_info u=m_current_user;
	u.password="";
	return u;
}



int simple_strategy::handler( WPARAM wParam )
{
	tthread::lock_guard<tthread::mutex> slock(m_dataQueueMutex);
	switch (wParam)
	{
	case LoginFinished:
		{
			while (!m_qUsers.empty())
			{
				on_login(m_qUsers.front());
				m_qUsers.pop();
			}
			break;
		}
	case LogoutFinished:
		{
			while (!m_qUsers.empty())
			{
				on_login(m_qUsers.front());
				m_qUsers.pop();
			}
			break;
		}
	case RtnDepthMarketData:
		{
			while (!m_qRealtime.empty())
			{
				on_report(m_qRealtime.front());
				m_qRealtime.pop();
			}
			break;
		}
	case RtnOrder:
		{
			//报单回报;
			while (!m_qOrders.empty())
			{
				on_order(m_qOrders.front());
				m_qOrders.pop();
			}
			break;
		}
	case RtnTrade:
		{
			//成交回报;
			while (!m_qTrades.empty())
			{
				on_trade(m_qTrades.front());
				m_qTrades.pop();
			}
			break;
		}
	case QryInstruments:
		{
			while (!m_qBaseData.empty())
			{
				basedata_t basedata=m_qBaseData.front();
				std::vector<std::string>::iterator iiter=
					std::find(m_current_user.instruments.begin(),
					m_current_user.instruments.end(),basedata.szCode);
				if (iiter!=m_current_user.instruments.end())
				{
					//是自己查询的合约;
					on_info(basedata);
				}
				m_qBaseData.pop();
			}
			break;
		}
	case RtnInstrumentStatus:
		{
			while (!m_qStatus.empty())
			{
				on_status(m_qStatus.front());
				m_qStatus.pop();
			}
			break;
		}
	default:
		break;
	}
	return 0;
}

std::vector<std::string>& simple_strategy::get_reg_markets()
{
	tthread::lock_guard<tthread::mutex> slock(m_mutex);
	return m_current_user.instruments;
}

void simple_strategy::on_trade( Trade& trade )
{
	//成将回报事件;
	DEBUG_METHOD();
}

void simple_strategy::on_order( OrderField& order )
{
	//委托回报事件;
	DEBUG_METHOD();
	INSTRUMENTOWN inst_own;
	strcpy(inst_own.szCode,order.InstrumentID.c_str());
	inst_own.dwShare=order.VolumeTotalOriginal;
	inst_own.dBuyPrice=order.LimitPrice;
	strcpy(inst_own.szMarket,order.ExchangeID.c_str());
	inst_own.direct=order.Direction;
	m_InstrumentsOwn.push_back(inst_own);
}


std::string simple_strategy::show()
{
	DEBUG_METHOD();
	ostringstream oss;
	oss<<"--------当前策略状态 BEGIN------"<<std::endl;
	DEBUG_MESSAGE("--------当前策略状态 BEGIN------");
	oss<<"用户:"<<m_current_user.user_id<<",经纪公司:"<<m_server.server_id<<std::endl;
	DEBUG_MESSAGE("用户:"<<m_current_user.user_id<<",经纪公司:"<<m_server.server_id);

	oss<<"--------持仓列表----------------"<<std::endl;
	DEBUG_MESSAGE("--------持仓列表----------------");
	for (std::size_t i=0;i<m_InstrumentsOwn.size();++i)
	{
		DEBUG_PRINTF("合约:%s,买卖方向:%d,手数:%d,价格:%f",m_InstrumentsOwn[i].szCode,
			m_InstrumentsOwn[i].direct,m_InstrumentsOwn[i].dwShare,
			m_InstrumentsOwn[i].dBuyPrice);
		oss<<"合约:"<<m_InstrumentsOwn[i].szCode<<",买卖方向:"<<m_InstrumentsOwn[i].direct<<",手数:"
			<<m_InstrumentsOwn[i].dwShare<<",价格:"<<m_InstrumentsOwn[i].dBuyPrice<<std::endl;
	}
	oss<<"------策略负载情况--------------"<<std::endl;
	DEBUG_MESSAGE("------策略负载情况--------------");

	DEBUG_PRINTF("队列状态:m_qRealtime:%d,m_qOrders:%d,m_qTrades:%d,m_qUsers:%d,m_qStatus:%d,m_qBaseData:%d",
		m_qRealtime.size(),m_qOrders.size(),m_qTrades.size(),
		m_qUsers.size(),m_qStatus.size());

	DEBUG_MESSAGE("--------行情--------------------");
	for (std::size_t j=0;j<m_InstrumentContainer.size();++j)
	{
		instrument_info info=m_InstrumentContainer.at(j);
		Tick rpt=info.m_reportLatest;
		DEBUG_PRINTF("%s,LastPrice:%f,Time:%s",rpt.szCode,rpt.LastPrice,DateTime(rpt.TradingTime,rpt.TradingMillisec).to_string().c_str());
	}
	
	DEBUG_MESSAGE("--------当前策略状态 END--------");

	return oss.str();
}


void simple_strategy::on_report( const Tick& rpt)
{
	DEBUG_METHOD();
	
	instrument_container::update(m_InstrumentContainer,&rpt,true);
	
	show();
}

Order simple_strategy::make_order( const std::string& uid, const std::string& instrument, EnumDirectionType direct,EnumOffsetFlagType posiDirect,double price,int volume )
{
	DEBUG_METHOD();
	DEBUG_PRINTF("下单:%s,%s,%s,%s,%f,%d",uid.c_str(),instrument.c_str(),FieldInterceptor::get_direction_type(direct).c_str(),
		FieldInterceptor::get_offset_flag_type(posiDirect).c_str(),price,volume);

	platform_server* op=
		(platform_server*)platform_manager::get_instance().FindByUser(uid);
	Order order;
	if (NULL==op)
	{
		return order;
	}
	
	order.BrokerID=op->get_server("trade").server_id;
	order.InvestorID=uid;
	order.PositionType=posiDirect;
	order.Direction=direct;
	order.Price=price;
	order.Volume=volume;
	order.InstrumentID=instrument;
	order.OrderPriceType=LimitPrice;

	int ret= op->request(IObserver::RspOrderInsert,&order);
	if (m_monitor)
	{
		m_monitor->on_make_order(uid,instrument,direct,posiDirect,price,volume);
	}
	return order;
}

void simple_strategy::set_monitor( strategy_monitor* pMonitor )
{
	m_monitor=pMonitor;
}

CAssetSerialContainer& simple_strategy::get_asset_serial()
{
	return m_AssetSerial;
}

COpRecordContainer& simple_strategy::get_op_record()
{
	return m_OpRecord;
}

CInstrumentOwnContainer& simple_strategy::get_instrument_own()
{
	return m_InstrumentsOwn;
}

int simple_strategy::position(const string& inst_id)
{
	int sum_pos=0;
	for (int i=0;i<m_InstrumentsOwn.size();i++)
	{
		if (m_InstrumentsOwn[i].szCode==inst_id)
		{
			if (m_InstrumentsOwn[i].direct==Buy)
			{
				sum_pos+=m_InstrumentsOwn[i].dwShare;
			}
			else
			{
				sum_pos-=m_InstrumentsOwn[i].dwShare;
			}
		}
	}
	return sum_pos;
}

std::string simple_strategy::name() const
{
	return "[simple_strategy]";
}

int simple_strategy::cancel_order(OrderField& order)
{
	//撤单;
	DEBUG_METHOD();
	DEBUG_PRINTF("撤单:%s,%s,%s,%f",order.UserID.c_str(),order.InstrumentID.c_str(),order.OrderSysID.c_str(),order.LimitPrice);
	platform_server* op=platform_manager::get_instance().Find(order.BrokerID);
	if (!op)
	{

	}
	OrderAction orderAction;
	orderAction.ActionFlag= DeleteAction;
	orderAction.BrokerID =order.BrokerID;
	orderAction.ExchangeID =order.ExchangeID;
	orderAction.FrontID = order.FrontID;
	orderAction.InstrumentID = order.InstrumentID;
	orderAction.InvestorID = order.InvestorID;
	orderAction.OrderRef = order.OrderRef;
	orderAction.OrderSysID = order.OrderSysID;
	orderAction.SessionID = order.SessionID;
	orderAction.RequestID = order.RequestID;

	int ret= op->request(IObserver::RspOrderAction,&orderAction);
	if (m_monitor)
	{
		m_monitor->on_cancel_order(order.InvestorID,order.InstrumentID);
	}
	return ret;
}

std::map<std::string,OrderField> simple_strategy::get_orders()
{
	return m_Orders;
}

int simple_strategy::close_all_position(const std::string& uid)
{
	int iret=NONE;
	CInstrumentOwnContainer& positions = get_instrument_own();
	for (size_t i=0;i<positions.size();i++)
	{
		if (positions[i].direct == Buy)
		{
			make_order(uid,positions[i].szCode,Sell,Close,positions[i].dBuyPrice,
				positions[i].dwShare);
		}
		else
		{
			make_order(uid,positions[i].szCode,Buy,Close,positions[i].dBuyPrice,
				positions[i].dwShare);
		}
	}
	return iret;
}

int simple_strategy::close_one_position( const std::string& uid,
										const std::string& iid, 
										double price,int volume /*=0*/ )
{
	int iret=NONE;
	CInstrumentOwnContainer& positions = get_instrument_own();
	for (size_t i=0;i<positions.size();i++)
	{
		if (positions[i].direct == Buy)
		{
			make_order(uid,positions[i].szCode,Sell,Close,price,
				volume==0?positions[i].dwShare:volume);
		}
		else
		{
			make_order(uid,positions[i].szCode,Buy,Close,price,
				volume==0?positions[i].dwShare:volume);
		}
	}
	return iret;
}


void login_thread_callback( void* strategy )
{
	DEBUG_METHOD();
	DEBUG_PRINTF("策略开始运行...发送登录请求...");
	simple_strategy* pStrategy=reinterpret_cast<simple_strategy*>(strategy);
	if (pStrategy)
	{
		//在这里进行用户登录;
		platform_server* plt_svr=platform_manager::get_instance().FindByUser(pStrategy->m_current_user.user_id,
			pStrategy->m_server.server_id);
		if (plt_svr)
		{
			pStrategy->m_current_user.trader_logined=plt_svr->get_user().trader_logined;
			pStrategy->m_current_user.market_logined=pStrategy->get_user().market_logined;
		}
		
		if (pStrategy->m_current_user.trader_logined && pStrategy->m_current_user.market_logined)
		{
			//用户已经登录,模拟登录成功;
			pStrategy->on_login(pStrategy->m_current_user);
		}
		else
		{
			platform_manager::get_instance().CreateOperator(pStrategy->m_current_user,&pStrategy->m_server);
		}
	}
}


void simple_strategy::on_start()
{
	login_thread_callback(this);
}

void simple_strategy::on_status( InstrumentStatus& status )
{

}

void simple_strategy::on_stop()
{

}

void simple_strategy::on_info(basedata_t& basedata)
{
	DEBUG_METHOD();
	DEBUG_PRINTF("订阅行情...");
	static bool bIsSubMarketData=false;
	if (m_InstrumentContainer.size()<m_current_user.instruments.size())
	{
		instrument_info info;
		instrument_info::update(info,&basedata);
		m_InstrumentContainer.add(info);
	}
	else
	{
		if (!bIsSubMarketData)
		{
			platform_server* op=platform_manager::get_instance().Find(m_current_user.broker_id);
			if (NULL==op)
			{
				return ;
			}
			int ret=op->request(IObserver::RtnDepthMarketData,&m_current_user.instruments);
			bIsSubMarketData=(ret==0);
		}
	}
}

bool simple_strategy::is_my_order( OrderField& order )
{
	if (m_current_user.user_id.empty())
	{
		return false;
	}
	if (m_current_user.ext_info.FrontID== order.FrontID && 
		m_current_user.ext_info.SessionID == order.SessionID)
	{
		return true;
	}
	return false;
}

bool simple_strategy::is_my_trade( Trade& trade )
{
	std::map<std::string,OrderField>::iterator oditer=m_Orders.begin();
	while (oditer!=m_Orders.end())
	{
		if (trade.OrderSysID==oditer->second.OrderSysID && trade.ExchangeID==
			oditer->second.ExchangeID)
		{
			return true;
		}
		++oditer;
	}
	return false;
}

bool simple_strategy::last_tick( Tick& rpt )
{
	instrument_info info;
	if (!m_InstrumentContainer.GetInstrumentInfo(rpt.szCode,&info))
	{
		return false;
	}
	rpt=info.m_reportLatest;
	return true;
}
