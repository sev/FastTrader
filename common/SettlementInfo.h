#ifndef _SETTLEMENT_INFO_H_
#define _SETTLEMENT_INFO_H_
#include <string>
//结算信息;

///投资者结算结果确认信息;
struct SettlementInfoConfirm
{
	///经纪公司代码;
	std::string	BrokerID;
	///投资者代码;
	std::string	InvestorID;
	///确认日期;
	std::string	ConfirmDate;
	///确认时间;
	std::string	ConfirmTime;
};

///投资者结算结果;
struct SettlementInfo
{
	///交易日;
	std::string	TradingDay;
	///结算编号;
	int	SettlementID;
	///经纪公司代码;
	std::string	BrokerID;
	///投资者代码;
	std::string	InvestorID;
	///序号;
	int	SequenceNo;
	///消息正文;
	std::string	Content;
};

#endif