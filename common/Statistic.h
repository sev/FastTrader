#ifndef _STATISTIC_H_
#define _STATISTIC_H_

#include <string>
#include <ctime>

#ifdef _WIN32
#pragma warning(disable : 4996)
#if _MSC_VER >=1700 
#include <mutex>
#include <chrono>
using namespace std;
#else
#include <boost/thread/mutex.hpp>
#include <boost/chrono/chrono.hpp>
using namespace boost;
#endif
#else
#include <mutex>
#include <chrono>
using namespace std;
#endif

#include <vector>
#include <tuple>

#ifdef HTTP_MONITOR
#define STAT_BEGIN(key) StatisticMgr::GetInstance().Begin((key))
#define STAT_BEGIN_MORE(key,desc) StatisticMgr::GetInstance().Begin((key),(desc))
#define STAT_FINISH(key) StatisticMgr::GetInstance().Finish(key)
#define STAT_FINISH_MORE(key,desc) StatisticMgr::GetInstance().Finish((key),(desc))
#define STAT_GET(key) StatisticMgr::GetInstance().Get((key))

#define STAT_DELETE(key) StatisticMgr::GetInstance().Delete((key))
#define STAT_CLEAR() StatisticMgr::GetInstance().Clear()
#else
#define STAT_BEGIN(key)  
#define STAT_BEGIN_MORE(key,desc)  
#define STAT_FINISH(key)  
#define STAT_FINISH_MORE(key,desc)  

#define STAT_DELETE(key)  
#define STAT_CLEAR()  
#define STAT_GET(key) ""
#endif
	
typedef std::tuple<std::string, chrono::high_resolution_clock::time_point,chrono::high_resolution_clock::time_point, 
	std::string,std::size_t> StatisticPoint;
struct StatisticPointPred
{
	std::string Key;
	StatisticPointPred(const std::string& key) :Key(key) {}
	bool operator()(const StatisticPoint& sp) {
		return std::get<0>(sp) == Key;
	}
};
class StatisticMgr
{
public:
	static StatisticMgr& GetInstance();
	void Begin(const std::string& key,const std::string& desc="");
	void Finish(const std::string& key, const std::string& desc = "");
	void Delete(const std::string& key);
	std::string Get(const std::string& key);
	void Clear();
protected:
	//时间统时点;
	std::vector<StatisticPoint> m_StatPts;
	StatisticMgr();
	StatisticMgr(const StatisticMgr& other);
	StatisticMgr& operator=(const StatisticMgr& other);
	mutex m_Mutex4Stat;
};
//有关于时间点的监测;
//策略统计;
struct StratigyStatistic
{
	//策略ID;
	std::string StrategyId;
	//启动时间;
	std::time_t StartedTime;
	//停止时间;
	std::time_t StoppedTime;
	//接收到的Tick数据;
	std::size_t TickCount;

	//OnOrder的处理时间;
	std::time_t OnOrderTime;
	//OnTrade的处理时间;
	std::time_t OnTradeTime;
	//OnTick的处理时间;
	std::time_t OnTickTime;

	StratigyStatistic()
	{
		StartedTime = 0;
		StoppedTime = 0;
		TickCount = 0;
		OnOrderTime = 0;
		OnTradeTime = 0;
		OnTickTime = 0;
	}
};



#endif
