#ifndef __FILE_HELPER_H__
#define __FILE_HELPER_H__

#include <stdlib.h>
#include <string>
#include <sstream>
#include <iomanip>
#ifdef _WIN32
#if _MSC_VER >=1700 
#include <chrono>
using namespace std;
#else
#include <boost/chrono/chrono.hpp>
using namespace boost;
#endif
#else
#include <chrono>
using namespace std;
#include<time.h>
#endif


#ifdef _WIN32
#include <direct.h>
#include <io.h>
#endif

inline std::string get_work_dir()
{
	std::string current_dir;
	char* buffer;
	// Get the current working directory: 
	if( (buffer = _getcwd( NULL, 0 )) == NULL )
	{
		//无法获取当前目录;
		current_dir="./";
	}
	else
	{
		current_dir=buffer;
		if (current_dir[current_dir.size()-1]!='\\' || current_dir[current_dir.size()-1]!='/')
		{
			current_dir.append("/");
		}
		free(buffer);
	}
	return current_dir;
}

//当前目录下;
//当前目录,如果没有，就创建目录;
inline bool create_dir(const std::string& subdir,std::string& fulldir)
{
	fulldir=get_work_dir();
	fulldir+= subdir;
	if (0 != _access(fulldir.c_str(), 0))
	{
		if(0!=_mkdir(fulldir.c_str()))
		{
			//目录创建失败;
			return false;
		}
	}
	return true;
}

//当前日期;
inline const std::string get_current_date_str()
{
	time_t tt = chrono::system_clock::to_time_t(chrono::system_clock::now());
	struct tm ptm={0};
#ifdef _MSC_VER
	localtime_s(&ptm,&tt);
#else
	memcpy(&ptm,localtime(&tt),sizeof(struct tm));
#endif
	//char date[32] = {0};
	stringstream ssdate;
	ssdate<<setw(4)<<setfill('0')<<(ptm.tm_year + 1900)
		<<setw(2)<<setfill('0')<<(ptm.tm_mon+1)
		<<setw(2)<<setfill('0')<<ptm.tm_mday;
	return ssdate.str();
}



#endif
