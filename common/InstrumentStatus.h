#ifndef _INSTRUMENT_STATUS_H_
#define _INSTRUMENT_STATUS_H_
#include <string>

//合约状态;
enum EnumInstStatusEnterReason:char
{
	///自动切换;
	IER_Automatic='1',
	///手动切换;
	IER_Manual='2',
	///熔断;
	IER_Fuse = '3',
};

enum EnumInstStatus: char
{
	///开盘前;
	IS_BeforeTrading='0',
	///非交易;
	IS_NoTrading='1',
	///连续交易;
	IS_Continous='2',
	///集合竞价报单;
	IS_AuctionOrdering='3',
	///集合竞价价格平衡;
	IS_AuctionBalance='4',
	///集合竞价撮合;
	IS_AuctionMatch='5',
	///收盘;
	IS_Closed='6',
};

struct InstStatus
{
	std::string ExchangeID;
	std::string SettlementGroupID;
	std::string InstrumentID;
	EnumInstStatus InstrumentStatus;
	int  TradingSegmentSN;
	std::string EnterTime;
	EnumInstStatusEnterReason EnterReason;
};


#endif