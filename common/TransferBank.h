#ifndef _TRANSFER_BANK_H_
#define _TRANSFER_BANK_H_

#include <string>

///转帐银行;
struct TransferBank
{
	///银行代码;
	std::string	BankID;
	///银行分中心代码;
	std::string	BankBrchID;
	///银行名称;
	std::string	BankName;
	///是否活跃;
	int	IsActive;
};

#endif