#ifndef _TICK_H_
#define _TICK_H_
#include "Enums.h"
#include <string>
#include <iostream>
#include <string.h>
#include <stdint.h>
#include <PlatformStruct.h>

#define DECLARE_TICK_INFO_T \
int64_t TickIndex; \
char	TradingDay[DATE_STRING_LENGTH]; \
double	LastPrice; \
double	PreSettlementPrice; \
double	PreClosePrice; \
double	PreOpenInterest; \
double	OpenPrice; \
double	HighestPrice; \
double	LowestPrice; \
int64_t	Volume; \
double	Turnover; \
double	OpenInterest; \
double	ClosePrice; \
double	SettlementPrice; \
double	UpperLimitPrice; \
double	LowerLimitPrice; \
time_t	UpdateTime; \
int	UpdateMillisec; \
char	UpdateTimeStr[TIME_STRING_LENGTH]; \
double	BidPrice[PRICE_VOLUME_LEVEL]; \
int64_t	BidVolume[PRICE_VOLUME_LEVEL]; \
double	AskPrice[PRICE_VOLUME_LEVEL]; \
int64_t	AskVolume[PRICE_VOLUME_LEVEL]; \
double	AveragePrice; \
double	PreDelta; \
double	CurrDelta;

struct tick_info_t
{
// 	///交易日;
// 	char	TradingDay[DATE_STRING_LENGTH]; \
// 		///最新价;
// 		double	LastPrice; \
// 		///上次结算价;
// 		double	PreSettlementPrice; \
// 		///昨收盘;
// 		double	PreClosePrice; \
// 		///昨持仓量;
// 		double	PreOpenInterest; \
// 		///今开盘;
// 		double	OpenPrice; \
// 		///最高价;
// 		double	HighestPrice; \
// 		///最低价;
// 		double	LowestPrice; \
// 		///数量;
// 		int64_t	Volume; \
// 		///成交金额;
// 		double	Turnover; \
// 		///持仓量;
// 		double	OpenInterest; \
// 		///今收盘;
// 		double	ClosePrice; \
// 		///本次结算价;
// 		double	SettlementPrice; \
// 		///涨停板价;
// 		double	UpperLimitPrice; \
// 		///跌停板价;
// 		double	LowerLimitPrice; \
// 		///最后修改时间;
// 		time_t	UpdateTime; \
// 		///最后修改毫秒;
// 		int	UpdateMillisec; \
// 		///交易所代码;
// 		char	UpdateTimeStr[TIME_STRING_LENGTH]; \
// 		///申买价一;
// 		double	BidPrice[PRICE_VOLUME_LEVEL]; \
// 		///申买量一;
// 		int64_t	BidVolume[PRICE_VOLUME_LEVEL]; \
// 		///申卖价一;
// 		double	AskPrice[PRICE_VOLUME_LEVEL]; \
// 		///申卖量一;
// 		int64_t	AskVolume[PRICE_VOLUME_LEVEL]; \
// 		///当日均价;
// 		double	AveragePrice; \
// 		///昨虚实度;
// 		double	PreDelta; \
// 		///今虚实度;
// 		double	CurrDelta;
	DECLARE_TICK_INFO_T
};

///深度行情;
struct Tick
{
	DECLARE_INSTRUMENT_BASE_T
	int64_t TickIndex; \
		char	TradingDay[DATE_STRING_LENGTH]; \
		double	LastPrice; \
		double	PreSettlementPrice; \
		double	PreClosePrice; \
		double	PreOpenInterest; \
		double	OpenPrice; \
		double	HighestPrice; \
		double	LowestPrice; \
		int64_t	Volume; \
		double	Turnover; \
		double	OpenInterest; \
		double	ClosePrice; \
		double	SettlementPrice; \
		double	UpperLimitPrice; \
		double	LowerLimitPrice; \
		time_t	UpdateTime; \
		int	UpdateMillisec; \
		char	UpdateTimeStr[TIME_STRING_LENGTH]; \
		double	BidPrice[PRICE_VOLUME_LEVEL]; \
		int64_t	BidVolume[PRICE_VOLUME_LEVEL]; \
		double	AskPrice[PRICE_VOLUME_LEVEL]; \
		int64_t	AskVolume[PRICE_VOLUME_LEVEL]; \
		double	AveragePrice; \
		double	PreDelta; \
		double	CurrDelta;
// 	Tick()
// 	{
// 		memset(this, 0, sizeof(Tick));
// 	}
// 	Tick(const Tick& tick)
// 	{
// 		static int CallTimes = 0;
// 		static time_t lastUpdateTime = this->UpdateTime;
// 		if (lastUpdateTime!= tick.UpdateTime)
// 		{
// 			lastUpdateTime = tick.UpdateTime;
// 			CallTimes = 1;
// 			//std::cout << "Tick 拷贝构造被调用 新的Tick" << std::endl;
// 		}
// 		else
// 		{
// 			CallTimes++;
// 			//std::cout << "Tick 拷贝构造被调用:"<< CallTimes << std::endl;
// 		}
// 		
// 		memcpy(this, &tick, sizeof(Tick));
// 	}
	std::string	InstrumentID() const { return szCode; }
	void InstrumentID(const std::string& id) { 
#ifdef _MSC_VER
		strncpy_s(szCode, id.c_str(), INSTRUMENT_ID_LENGTH);
#else
		strncpy(szCode, id.c_str(), INSTRUMENT_ID_LENGTH); 
#endif
	}
// 	Tick& operator=(const Tick& tick)
// 	{
// 		static int CallTimes = 0;
// 		static time_t lastUpdateTime = this->UpdateTime;
// 		if (lastUpdateTime != tick.UpdateTime)
// 		{
// 			lastUpdateTime = tick.UpdateTime;
// 			CallTimes = 1;
// 			std::cout << "Tick 复制操作符被调用 新的Tick" << std::endl;
// 		}
// 		else
// 		{
// 			CallTimes++;
// 			std::cout << "Tick 复制操作符被调用:" << CallTimes << std::endl;
// 		}
// 		memcpy(this, &tick, sizeof(tick));
// 		return *this;
// 	}
};
#endif
