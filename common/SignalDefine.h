#pragma once

#include <boost/signals2/signal.hpp>

struct Tick;

typedef boost::signals2::signal<void(boost::shared_ptr<Tick>)> OnTickSignal;
