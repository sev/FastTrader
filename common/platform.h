#ifndef PLATFORM_H
#define PLATFORM_H
#include <iostream>

#ifdef _WIN32
#include <windows.h>
#include <tchar.h>
#define PATH_SPLITTER '\\'
#else
#define PATH_SPLITTER '/'
#define EXTERN_C_API  extern "C"
#define DLL_EXT ".so"
#define APP_EXT ""
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <strings.h>
#include <chrono>
#include <thread>
#include <dlfcn.h>
#include <signal.h>
typedef unsigned long     LPARAM;
typedef unsigned int        WPARAM;
#define  _stricmp strcasecmp
typedef void* HMODULE;
#define FORWARD_DECL_ENUM(e) enum class e
#define _getcwd getcwd
#define _access access
#define _mkdir(path) mkdir(path,S_IRWXU)
#define Sleep(ms) std::this_thread::sleep_for(std::chrono::duration<double, std::milli>(ms));
#endif
#endif // PLATFORM_H

