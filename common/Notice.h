#ifndef _NOTICE_H_
#define _NOTICE_H_

#include <string>

///客户通知;
struct Notice 
{
	///经纪公司代码;
	std::string BrokerID;
	///消息正文;
	std::string Content;
	///经纪公司通知内容序列号;
	std::vector<std::string> SequenceLabel;
	//请求编号;
	int RequestID;
};



///用户事件通知;
struct TradingNotice
{
	///经纪公司代码;
	std::string			BrokerID;
	///投资者范围;
	EnumInvestorRange	InvestorRange;
	///投资者代码;
	std::string			InvestorID;
	///序列系列号;
	short				SequenceSeries;
	///用户代码;
	std::string			UserID;
	///发送时间;
	std::string     	SendTime;
	///序列号;
	int             	SequenceNo;
	///消息正文;
	std::string			FieldContent;
};

#endif