#include "BaseData.h"
#include <cassert>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>
using namespace std;





int BasedataContainer::insert_sort(boost::shared_ptr<BASEDATA> newElement )
{
	size_t i = 0;
	for( ; i<size(); i++ )
	{
		boost::shared_ptr<BASEDATA>	& temp = at(i);
		if( temp->m_date == newElement->m_date )
		{
			at(i)=newElement;
			return i;
		}
		if( temp->m_date > newElement->m_date )
		{
			insert(i+begin(),newElement);
			return i;
		}
	}
	push_back( newElement );
	return i;
}

void BasedataContainer::sort()
{
	std::sort(begin(), end(), 
	[](boost::shared_ptr<BASEDATA> pTemp1, boost::shared_ptr<BASEDATA> pTemp2)
	{
		return pTemp1->m_date < pTemp2->m_date;
	});
}

