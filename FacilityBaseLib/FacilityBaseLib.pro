TARGET = FacilityBaseLib
TEMPLATE = lib
INCLUDEPATH += $$PWD/../sdk/include
INCLUDEPATH += ../common
INCLUDEPATH += /home/rmb338/boost_1_64_0
INCLUDEPATH += ../spdlog/include
DEFINES += _USE_SPDLOG
CONFIG -= qt
CONFIG += debug_and_release

linux-g++{
QMAKE_LFLAGS= -m64 -Wall -DNDEBUG  -O2
QMAKE_CFLAGS = -arch x86_64 -lpthread -lstdc++11
}
CONFIG(debug, debug|release) {
        DESTDIR = ../build/debug
} else {
        DESTDIR = ../build/release
}

QMAKE_LFLAGS += /home/rmb338/boost_1_64_0/stage/lib/libboost_system.a
QMAKE_LFLAGS += /home/rmb338/boost_1_64_0/stage/lib/libboost_filesystem.a
QMAKE_LFLAGS += /home/rmb338/boost_1_64_0/stage/lib/libboost_thread.a
QMAKE_LFLAGS += /home/rmb338/boost_1_64_0/stage/lib/libboost_serialization.a
QMAKE_LFLAGS += /home/rmb338/boost_1_64_0/stage/lib/libboost_date_time.a

DEFINES *= FACILITYLIB_EXPORTS
HEADERS += \
    array_container.h \
    StringMgr.h \
    Report.h \
    Minute.h \
    KData.h \
    InstrumentInfo.h \
    InstrumentConfig.h \
    facilitybaselib.h \
    Express.h \
    Container.h \
    BaseData.h \
    plate.h \
    InstrumentData.h \
    ExchangeBase.h \
    ExchangeContainer.h

SOURCES += \
    StringMgr.cpp \
    Report.cpp \
    Minute.cpp \
    KData.cpp \
    InstrumentInfo.cpp \
    InstrumentConfig.cpp \
    IMCode.cpp \
    faciliytbaselib.cpp \
    Express.cpp \
    Container.cpp \
    BaseData.cpp \
    plate.cpp \
    InstrumentData.cpp \
    ExchangeContainer.cpp \
    ExchangeBase.cpp
