#ifndef _INSTRUMENT_INFO_H_
#define _INSTRUMENT_INFO_H_
#include "KData.h"
#include "BaseData.h"
#include "Minute.h"
#include "Tick.h"
#include <boost/shared_ptr.hpp>
#define	PROG_PROGRESS			1
#define	STKLIB_MAX_PROGRESS		10000
#define	STKLIB_MAXF_PROGRESS		10000.


typedef bool (* PROGRESS_CALLBACK)(DWORD dwCode, DWORD dwProgress, const char* lpszMsg, void * cookie);
//单个合约的信息;
//在程序中用于传递数据,和一些指标的计算;
class FACILITY_API InstrumentInfo:
	public instrument_base_t,
	public base_info_t,
	public tick_info_t
{
public:
	InstrumentInfo();
	InstrumentInfo(const InstrumentInfo& src);
	void clear();
	InstrumentInfo& operator=(const InstrumentInfo& si);
	bool is_equal(const char* exID,const char* szID);
	bool is_equal(DWORD dwMarket,const char* szID);
	int	get_type();
	void set_type(int type);
	const char* GetExchangeID();
	void SetExchangeID(const char* exID);
	const char* get_id();
	bool set_id(const char* exID,const char* szID);
	const char* get_name();
	void set_name(const char* szName);

	void ResolveTypeAndMarket();
	bool is_valid();
	int GetAccuracy();
	bool is_stock_index() const;//是否为股指期货;

	void SetShortName(const char* szShortName);
	const char* GetShortName();
	void SetDomain(const std::string& szDomain);
	std::string GetDomain();

	void SetNameEnu(const char* szNameEnu);
	const char* GetNameEnu();

	void SetNameChs(const char* szName);
	const char* GetNameChs();
public:
	//添加自定义方法;
	bool	StatTechIndex( DWORD dwDate );	// 设定当前技术资料日期，Format is YYYYMMDD;
	bool	StatBaseIndex( DWORD dwDate );	// 设定当前基本资料日期，Format is YYYYMMDD;
	//新增的函数;
 	bool GetAverage(double* pValue);//当日成交均价;
 	bool GetDiff(double *pValue,DWORD dateCur,int nDays);//涨跌;
	double GetDiff(int nDays=1);
	double GetDiffPercent(int nDays=1);

 	bool GetDiffPercent(double* pValue,DWORD dateCur,int nDays);//涨跌幅度;
 	bool GetScope(double* pValue,DWORD dateCur,int nDays);//涨跌范围;
 	bool GetDiffPercentMin5(double* pValue);

 	bool GetShareCurrency(double* pValue);
// 	bool GetRatioCurrency(double* pValue);
 	bool GetRatioChangeHand(double* pValue,double dVolume);
 	bool GetRatioChangeHand(KdataContainer& kdata,double* pValue,DWORD dateCur,int nDays);
 	bool GetRatioChangeHand(double* pValue,DWORD dateCur,int nDays);

	bool	GetRatioVolume( double * pValue, DWORD dateCur, int nDays );		// n日量比;
	bool	GetRS( double * pValue, DWORD dateCur, int nDays );					// n日相对强度;
	bool	GetSellBuyRatio( double *pdRatio, double *pdDiff );					// 委比和委差;
	//转换成Instrument;
	boost::shared_ptr<Instrument> toInstrument();

	static std::vector<std::string>& get_type_names();

	inline bool	market_value( double * pValue );	// 市值,期货是价值;
	// 基本信息更新;
	static bool update(InstrumentInfo& info, boost::shared_ptr<BASEDATA> pBasedata);
	static bool update(InstrumentInfo& info, Instrument* pInstrument);
	// 行情更新;
	static bool update(InstrumentInfo& info,const Tick * pReport);
	// 分钟数据更新;
	static bool update(InstrumentInfo& info,MINUTE* pMinute);
public:
	KdataContainer m_kdata;
	BasedataContainer m_basedata;
	boost::shared_ptr<MinuteContainer> m_minute;
	Tick m_reportLatest;
	//合约当前状态;
	boost::shared_ptr<InstStatus> m_status;
public:
	//自添加(自定义)的字段;
	DWORD	m_datebase;					//日期	Format is YYYYMMDD for base data;
	DWORD	m_datetech;	//日期	Format is XXMMDDHHMM for 5min, Format is YYYYMMDD for day;
	DWORD   m_type;
	char m_szNameEnu[32];	//英文名称;
	char m_szShortName[16];	//简称;
	std::string m_szDomain;	//板块;
public:
	DWORD m_datebegin;

	DWORD	m_dwAdvance;		// 仅指数有效;
	DWORD	m_dwDecline;		// 仅指数有效;
};


string AfxMakeSpellCode( string & strSrc, int nMode = 0x0001 );
bool FACILITY_API convert_REPORT_to_MINUTE(const Tick * pReport, MINUTE * pMinute );
bool FACILITY_API convert_MINUTE_to_REPORT(  MINUTE * pMinute,Tick * pReport );
bool FACILITY_API convert_KDATA_to_REPORT(const KDATA * pKD, Tick * pReport);
bool FACILITY_API UpdateKDATAByREPORT( KDATA &kd,const Tick * pReport );
#endif
