#include "Market.h"
#include "../Log/logging.h"
#include "file_helper.h"
#include <iostream>
#include <algorithm>
#include <tuple>
#include <string.h>
#include <iterator>
//#include "../DateTime/DateTime.h"
#ifdef _MSC_VER
#pragma warning(disable : 4996)
#if _MSC_VER >=1700 
#include <chrono>
using namespace std;
#else
#include <boost/chrono/chrono.hpp>
using namespace boost;
#endif
#else
#include <chrono>
using namespace std;
#endif
#include "../common/Statistic.h"

Market::Market()
{
	LOGINIT("market");
	m_bFrontDisconnected=false;
	m_bIsLogined=false;
	m_nRequestID = 0;
}

int& Market::RequestID()
{
	return m_nRequestID;
}



const ErrorMessage& Market::LastError() const
{
	return m_ErrorMessage;
}

void Market::UpdateSubInst(const std::string& inst,const std::string& exchg,bool bAddOrDel)
{
	unique_lock<mutex> lck(m_MutInst);
	auto fnFind=[&inst,&exchg](const std::map<std::string,std::vector<std::string> >::value_type& v)->bool{
		if (v.first!=exchg)
		{
			return false;
		}
		auto iIter=std::find(v.second.begin(),v.second.end(),inst);
		if (iIter==v.second.end())
		{
			return false;
		}
		else
		{
			return true;
		}
	};
	auto iiter = std::find_if(m_SubInstruments.begin(), m_SubInstruments.end(), 
		fnFind);
	if (bAddOrDel)
	{
		if (iiter == m_SubInstruments.end())
		{
			std::vector<std::string> insts(1,inst);
			m_SubInstruments[exchg]=insts;
		}
	}
	else
	{
		if (iiter!=m_SubInstruments.end())
		{
			m_SubInstruments.erase(iiter);
		}
	}
}


bool Market::SubFilter(const std::vector<std::string>& instruments,const std::string& exchg,std::vector<std::string>& unSubInstruments)
{
	if (instruments.empty())
	{
		return false;
	}
	//获取没有订阅的;
	unique_lock<mutex> lck(m_MutInst);
	auto instIter=m_SubInstruments.find(exchg);
	if (instIter==m_SubInstruments.end())
	{
		unSubInstruments=instruments;
// 		std::copy_if(instruments.begin(), instruments.end(), std::back_inserter(unSubInstruments), 
// 			[](const std::string& inst_id){return !inst_id.empty(); });
	}
	else
	{
		std::vector<std::string> copyed_insts;

		std::copy_if(instruments.begin(), instruments.end(), std::back_inserter(copyed_insts),
			[](const std::string& inst_id){return !inst_id.empty(); });

		std::sort(copyed_insts.begin(), copyed_insts.end());
		std::sort(instIter->second.begin(), instIter->second.end());

		std::set_difference(copyed_insts.begin(), copyed_insts.end(),
			instIter->second.begin(),instIter->second.end(),
			std::back_inserter(unSubInstruments)
			);
	}
 	return !unSubInstruments.empty();
}

bool Market::UnSubFilter(const std::vector<std::string>& instruments,const std::string& exchg,std::vector<std::string>& unSubInstruments)
{
	if (instruments.empty())
	{
		return false;
	}
	unique_lock<mutex> lck(m_MutInst);
	//获取已经订阅的;
	auto instIter=m_SubInstruments.find(exchg);
	if (instIter==m_SubInstruments.end())
	{
		return true;
	}
	else
	{
		//都有的，才退订;
		auto copyed_instruments = instruments;
		std::sort(copyed_instruments.begin(), copyed_instruments.end());
		std::sort(instIter->second.begin(), instIter->second.end());
		std::set_intersection(copyed_instruments.begin(), copyed_instruments.end(),
			instIter->second.begin(),instIter->second.end(),
			std::inserter(unSubInstruments, unSubInstruments.begin())
			);
	}
// 	//去重;
// 	std::sort(unSubInstruments.begin(), unSubInstruments.end()); 
// 	auto last = std::unique(unSubInstruments.begin(), unSubInstruments.end());
// 	unSubInstruments.erase(last, unSubInstruments.end());
 	return !unSubInstruments.empty();
}

bool Market::Init( const ServerInfo& s )
{
	return false;
}

bool Market::Login( const UserLoginParam& u )
{
	return m_bIsLogined;
}

bool Market::Logout()
{
	return false;
}




bool Market::ReqUserLogin()
{
	return true;
}


bool Market::SubscribeMarketData(const std::vector<std::string>& instruments,const std::string& exchg)
{
	return false;
}


std::string Market::UserID() const
{
	return m_UserInfo.UserID;
}

std::string Market::BrokerID() const
{
	return m_UserInfo.BrokerID;
}

std::string Market::GetTradingDay()
{
	return "";
}

std::string Market::GetApiVersion()
{
	return "";
}

bool Market::UnSubscribeMarketData( const std::vector<std::string>& instruments ,const std::string& exchg)
{
	return false;
}

void Market::GetSubInstruments(std::map<std::string,std::vector<std::string> >& instruments)
{
	unique_lock<mutex> lck(m_MutInst);
	instruments = m_SubInstruments;
}

Market::~Market()
{
}

std::string Market::Id() const
{
#ifdef _MSC_VER
#if _MSC_VER>=1700
	std::string thisid= std::to_string(size_t(this));
#else
	std::string thisid= std::to_string((unsigned long long)size_t(this));
#endif
#else
    std::string thisid= std::to_string((unsigned long long)size_t(this));
#endif
	return thisid;
}



