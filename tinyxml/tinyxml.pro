TEMPLATE = lib
CONFIG -= qt
TARGET = tinyxml


CONFIG(debug,debug|release){
    DEFINES +=_DEBUG
    DESTDIR  =../build/debug/
    LIBS += -L../build/debug/
} else {
    DESTDIR = ../build/release/
    LIBS += -L../build/release/
}
macx{
    QMAKE_MAC_SDK = macosx10.11
}

HEADERS += \
    tinyxml.h \
    tinystr.h

SOURCES += \
    tinyxml.cpp \
    tinystr.cpp \
    tinyxmlerror.cpp
